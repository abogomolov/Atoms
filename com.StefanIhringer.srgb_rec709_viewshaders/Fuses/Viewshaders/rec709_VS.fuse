--[[
Simple linear-to-ITU-R BT.709 (HD Gamut) viewshader with gain/gamma sliders by Stefan Ihringer. Based on GainGammaVS.fuse by eyeon 

This shader does not convert white points or primaries. It simply adds the required gamma curve.

Updated to universal fuse with Fusion/Resolve 17+ support by Alexey Bogomolov. For conversion reference see GLSL utility functions by https://github.com/tobspr/GLSL-Color-Spaces
Email: mail@abogomolov.com
Donate: paypal.me/aabogomolov/3usd

]]--

-- params for the shader:

params = 
[[
float gain
float gamma
]]

local FuVersion = tonumber(bmd._VERSION:sub(1,2))

-- GLSL shader:
local shaderGLSL = 
[[
//begin=glsl//              // vim syntax highlight

const float REC709_ALPHA = 0.099f;

float linear_to_rec709(float channel) {
    if(channel <= 0.018f)
        return 4.5f * channel;
    else
        return (1.0 + REC709_ALPHA) * pow(channel, 0.45f) - REC709_ALPHA;
}

vec3 rgb_to_rec709(vec3 rgb) {
    return vec3(
        linear_to_rec709(rgb.r),
        linear_to_rec709(rgb.g),
        linear_to_rec709(rgb.b)
    );
}

void ShadePixel(inout FuPixel f)
{
	EvalShadePixel(f);								// get source pixel
    f.Color.rgb *= gain;
    f.Color.rgb = sign(f.Color.rgb) * pow(abs(f.Color.rgb), vec3(gamma));
	// convert to ITU-R BT.709
    f.Color.rgb = rgb_to_rec709(f.Color.rgb);

}
//end=glsl//
]]

-- Cg Shader
local shaderCG = 

[[
//begin=glsl//
struct VSrec709 : ViewShader
{
	ViewShader source;					// need this line
	float gain, gamma;					// external parameters

	void ShadePixel(inout FuPixel f)
	{
		source.ShadePixel(f);			// get source pixel
		
		f.Color.rgb *= gain;
		f.Color.rgb = sign(f.Color.rgb) * pow(abs(f.Color.rgb), gamma);
		// convert result to ITU-R BT.709
		f.Color.rgb = f.Color.rgb > 0.018 ? 1.099f * pow(f.Color.rgb, 0.45f) - 0.099f : 4.5f * f.Color.rgb;
	}
};
//end=glsl//
]]

FuRegisterClass("VSrec709", CT_ViewLUTPlugin, {		-- ID must be unique
	REGS_Name = "rec709",
    REG_Fuse_NoEdit   = false,
	REG_Fuse_NoReload = false,}
)

-- Called on creation. Add any controls here.
function Create()
	InGain = self:AddInput("Gain", "Gain", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinScale = 0.0,
		INP_MaxScale = 5.0,
		INP_Default  = 1.0,
		ICD_Center = 1.0,
		})
	InGamma = self:AddInput("Gamma", "Gamma", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinAllowed = 0.001,
		INP_MaxScale = 5.0,
		INP_Default  = 1.0,
		ICD_Center = 1.0,
		})
end

if FuVersion >= 17 then
    shader = shaderGLSL

else
    shader = shaderCG
end

-- This is called when the shader is created in Fusion 17+
function SetupShadeNode(group, req, img)
        return ViewShadeNode(group, "VSrec709", params, shader)
    end

    -- This is called when the shader is created in Fusion v < 17
function SetupShader(req, img)
    local vs = ViewShader("VSrec709", shader);		-- pass struct name and shader string
    vs:AddParam("gain");
    vs:AddParam("gamma");
    return vs;
end

-- This is called every display refresh
-- img may be nil
function SetupParams(req, vs, img)
	local gain = InGain:GetValue(req).Value
	local gamma = InGamma:GetValue(req).Value

	gamma = 1.0 / gamma
    if FuVersion >= 17 then
        vs:Set(1, gain)
        vs:Set(2, gamma)
    else
        vs:SetParam(1, gain);
        vs:SetParam(2, gamma);
    end
	return true
end
