ui = fu.UIManager
disp = bmd.UIDispatcher(ui)

dlg = disp.AddWindow({"WindowTitle": "My First Window", "ID": "MyWin", "Geometry": [100, 100, 400, 45], "Spacing": 10,},[
	ui.VGroup({"ID": "root",},[
		# Add your GUI elements here:
		ui.ComboBox({"ID": "MyCombo", "Text": "Combo Menu"}),
	]),
])

itm = dlg.GetItems()

# The window was closed
def _func(ev):
	disp.ExitLoop()
dlg.On.MyWin.Close = _func

# Add your GUI element based event functions here:

def _func(ev):
	if itm['MyCombo'].CurrentIndex == 0:
		print('[' + itm['MyCombo'].CurrentText + '] Lets make an apple crisp dessert.')
	elif itm['MyCombo'].CurrentIndex == 1:
		print('[' + itm['MyCombo'].CurrentText + '] Lets make a banana split with ice cream')
	elif itm['MyCombo'].CurrentIndex == 2:
		print('[' + itm['MyCombo'].CurrentText + '] Lets make some cherry tarts.')
	elif itm['MyCombo'].CurrentIndex == 3:
		print('[' + itm['MyCombo'].CurrentText + '] Lets peel an orange and have sliced orange boats.')
	elif itm['MyCombo'].CurrentIndex == 4:
		print('[' + itm['MyCombo'].CurrentText + '] Lets eat cubed mango chunks with yoghurt.')
	elif itm['MyCombo'].CurrentIndex == 5:
		print('[' + itm['MyCombo'].CurrentText + '] Lets have a fresh Kiwi snack.')
dlg.On.MyCombo.CurrentIndexChanged = _func

# Add the items to the ComboBox menu
itm['MyCombo'].AddItem("Apple")
itm['MyCombo'].AddItem("Banana")
itm['MyCombo'].AddItem("Cherry")
itm['MyCombo'].AddItem("Orange")
itm['MyCombo'].AddItem("Mango")
itm['MyCombo'].AddItem("Kiwi")

dlg.Show()
disp.RunLoop()
dlg.Hide()
