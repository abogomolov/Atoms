# Print out the control page names for the selected node.

if comp.ActiveTool:
	print(comp.ActiveTool.Name + " Node Control Pages:")
	pages = comp.ActiveTool.GetControlPageNames()
	for p in pages:
		print("\t" + pages[p])
	print("\n")
else:
	print("Please select a node and run this script again\n")
