-- Note: Fusion clamps numbers over 1 million in the number datatype.

-- ============================================================================
-- modules
-- ============================================================================
local filesystemutils = self and require("vfilesystemutils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vFileSystemFileSize"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\FileSystem",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the file size.",
    REGS_OpIconString = FUSE_NAME,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text", "Text", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })

    InUnit = self:AddInput("Unit", "Unit", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ComboControl",
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = "Byte (B)"},
        {CCS_AddString = "Kilobyte (KB)"},
        {CCS_AddString = "Kibibyte (KiB)"},
        {CCS_AddString = "Megabyte (MB)"},
        {CCS_AddString = "Mebibyte (MiB)"},
        {CCS_AddString = "Gigabyte (GB)"},
        {CCS_AddString = "Gibibyte (GiB)"},
        {CCS_AddString = "Terabyte (TB)"},
        {CCS_AddString = "Tebibyte (TiB)"},
        CC_LabelPosition = "Vertical"
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })

    OutUnit = self:AddOutput("OutputUnit" , "OutputUnit" , {
        LINKID_DataType = "Text",
        LINK_Main = 2
    })
end

function Process(req)
    -- [[ Creates the output. ]]
    local path = InText:GetValue(req).Value

    local unit = InUnit:GetValue(req).Value

    local size = 0
    local unitname = ""
    -- print("[Unit]", unit)

    if unit == 0 then
        -- Byte
        size = filesystemutils.file_size(path)
        unitname = "B"
    elseif unit == 1 then
        -- Kilobyte
        size = filesystemutils.file_size(path) / 1000
        unitname = "KB"
    elseif unit == 2 then
        -- Kibibyte
        size = filesystemutils.file_size(path) / 1024
        unitname = "KiB"
    elseif unit == 3 then
        -- Megabyte
        size = filesystemutils.file_size(path) / 1000000
        unitname = "MB"
    elseif unit == 4 then
        -- Mebibyte
        size = filesystemutils.file_size(path) / 1048576
        unitname = "MiB"
    elseif unit == 5 then
        -- Gigabyte
        size = filesystemutils.file_size(path) / 1000000000
        unitname = "GB"
    elseif unit == 6 then
        -- Gibibyte
        size = filesystemutils.file_size(path) / 1073741824
        unitname = "GiB"
    elseif unit == 7 then
        -- Terabyte
        size = filesystemutils.file_size(path) / 1000000000000
        unitname = "TB"
    elseif unit == 8 then
        -- Tebibyte
        size = filesystemutils.file_size(path) / 1099511627776
        unitname = "TiB"
    else
        -- Fall back is Kilobyte
        size = filesystemutils.file_size(path) / 1000
        unitname = "KB"
    end

    -- print("[File Size]", size)

    if size > 1000000 then
        print("[Warning] The FileSize value is over 1 million and is likely clipped by the default Number datatype range in Fusion. You may have to add a INP_MaxAllowed = 1e+38, parameter to the number control on your connected node.")
        print("[File Size]", size)
    end

    OutText:Set(req, Number(size))
    OutUnit:Set(req, Text(unitname))
end
