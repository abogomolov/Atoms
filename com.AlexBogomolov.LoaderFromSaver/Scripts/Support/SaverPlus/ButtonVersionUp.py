import re
import os


def version_up(tool):
    comp.Lock()
    comp.StartUndo("Version Up")
    output_old = tool.Clip[1]
    if output_old == "":
        print("No filename specified in saver")
        return
    pattern = re.compile(r"([Vv])(\d{2,})")

    try:
        v_letter, number = re.search(pattern, output_old).groups()
    except AttributeError:
        print("No version pattern found.")
        return

    start_count = int(number)
    version_new_number = str(start_count + 1).zfill(len(number))
    new_path = re.sub(pattern, "{}{}".format(v_letter, version_new_number), output_old)
    tool.Clip[1] = new_path
    print("Saver [{}] new filename: [{}]".format(tool.Name, os.path.basename(new_path)))
    comp.EndUndo()
    comp.Unlock()


if __name__ == "__main__":
    comp = fu.GetCurrentComp()
    tool = comp.ActiveTool
    if tool and tool.ID == "Saver":
        version_up(tool)
