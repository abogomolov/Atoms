function check_selected(tool)
    return tool:GetAttrs('TOOLB_Selected')
end

function check_enabled(tool)
    return tool:GetAttrs('TOOLB_PassThrough')
end

local comp = fu:GetCurrentComp()
local selectedSavers = comp:GetToolList(true, "Saver")
local allSavers = comp:GetToolList(false, "Saver")

comp:StartUndo("Solo Saver")

for _, currentSaver in pairs(allSavers) do
    if not check_selected(currentSaver) then
        currentSaver:SetAttrs( { TOOLB_PassThrough = true } )
    end
end

for _, sel in pairs(selectedSavers) do
    if check_enabled(sel) then
        sel:SetAttrs({ TOOLB_PassThrough = false })
    end
end
comp:EndUndo()