"""
This tool script disables the currently selected saver, disconnects it and adds a new Saver Plus.
It also checks if the default saver settings already exist. If default settings are found, it is assumed that the SaverPlus settings are apready saved as default.
In this case default settings will be applied automatically when new tool is created. Otherwise new controls would be applied to new Saver.
Old saver will be renamed to <saver-name>_old, disconnected and disabled
New saver will be reconnected and will inherit the name and all properties of the old saver.

Copyright © 2023 Alexey Bogomolov

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from pathlib import Path


def update_user_controls(user_controls):

    print("Updating the saver inputs")

    user_controls["SOLO"] = {
        "LINKID_DataType": "Number",
        "INP_Default": 0,
        "INPID_InputControl": "ButtonControl",
        "BTNCS_Execute": ' comp:RunScript("Scripts:Support/SaverPlus/SoloSaver.lua") ',
        "LINKS_Name": "Solo",
        "ICS_ControlPage": "File",
    }
    user_controls["ML"] = {
        "LINKID_DataType": "Number",
        "INP_Default": 0,
        "INPID_InputControl": "ButtonControl",
        "BTNCS_Execute": ' tool = comp.ActiveTool; comp:RunScript("Scripts:Comp/Saver Tools/LoaderFromSaver.lua", tool) ',
        "LINKS_Name": "Make Loader",
        "ICS_ControlPage": "File",
    }
    user_controls["VersionUP"] = {
        "LINKID_DataType": "Number",
        "INP_Default": 0,
        "INPID_InputControl": "ButtonControl",
        "BTNCS_Execute": ' tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionUp.py", tool) ',
        "LINKS_Name": "Version UP",
        "ICS_ControlPage": "File",
    }
    user_controls["VersionDOWN"] = {
        "LINKID_DataType": "Number",
        "INP_Default": 0,
        "INPID_InputControl": "ButtonControl",
        "BTNCS_Execute": ' tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionDown.py", tool) ',
        "LINKS_Name": "Version DOWN",
        "ICS_ControlPage": "File",
    }

    return user_controls


def fusion_has_defaults():
    for segment in fu.MapPathSegments("defaults:").values():
        segment = Path(segment)
        for setting in segment.rglob("*.setting"):
            if setting.name == "Saver_Saver.setting":
                print("Found default saver settings in ", segment)
                return True
    print("Default settings not found, updating current saver")
    return False


def reconnect_savers(old_saver, new_saver) -> None:
    print("Reconnecting Savers: ", old_saver.Name, new_saver.Name)
    oldMainInput = old_saver.FindMainInput(1)
    connectedToolOutput = oldMainInput.GetConnectedOutput()
    if connectedToolOutput:
        new_inp = new_saver.FindMainInput(1)
        new_inp.ConnectTo(connectedToolOutput)
    oldMainOutput = old_saver.FindMainOutput(1)
    if oldMainOutput:
        newMainOutput = new_saver.FindMainOutput(1)
        connectedInputs = oldMainOutput.GetConnectedInputs()
        for inp in connectedInputs.values():
            inp.ConnectTo(newMainOutput)
    oldMainInput.ConnectTo()


def rename_savers(old_saver, new_saver) -> None:
    old_tool_name = old_saver.Name
    old_saver.SetAttrs({"TOOLB_NameSet": True, "TOOLS_Name": f"{old_tool_name}_old"})
    new_saver.SetAttrs({"TOOLB_NameSet": True, "TOOLS_Name": old_tool_name})
    old_saver.SetAttrs({"TOOLB_PassThrough": True})


def process():
    if not tool:
        print("This is a tool script")
        return

    if not tool.ID == "Saver":
        print("This is not a Saver")
        return

    settings = comp.CopySettings(tool)
    comp.Lock()
    comp.StartUndo("Convert to saver_plus")
    saver_plus = comp.AddTool("Saver", -32768, -32768)

    comp.SetActiveTool(saver_plus)

    if not fusion_has_defaults():
        user_controls = saver_plus.UserControls
        updated_controls = update_user_controls(user_controls)
        saver_plus.UserControls = updated_controls

    saver_plus.LoadSettings(settings)

    rename_savers(tool, saver_plus)
    reconnect_savers(tool, saver_plus)

    saver_plus.Refresh()
    saver_plus = comp.ActiveTool
    saver_plus.LoadSettings(settings)

    comp.EndUndo()
    comp.Unlock()


process()
