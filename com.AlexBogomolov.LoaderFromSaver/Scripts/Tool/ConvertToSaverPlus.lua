--[[
This tool script disables the currently selected saver, disconnects it and adds a new Saver Plus.
It also checks if the default saver settings already exist. If default settings are found, it is assumed that the SaverPlus settings are apready saved as default.
In this case default settings will be applied automatically when new tool is created. Otherwise new controls would be applied to new Saver.
Old saver will be renamed to <saver-name>_old, disconnected and disabled
New saver will be reconnected and will inherit the name and all properties of the old saver.

Copyright © 2023 Alexey Bogomolov

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE
OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
]]


function UpdateUserControls(ctrls)
    
    print("Updating the saver inputs")
    
    ctrls.SOLO = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ comp:RunScript("Scripts:Support/SaverPlus/SoloSaver.lua") ]],
        LINKS_Name = "Solo",
        ICS_ControlPage = "File",
    }
    ctrls.ML = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Comp/Saver Tools/LoaderFromSaver.lua", tool) ]],
        LINKS_Name = "Make Loader",
        ICS_ControlPage = "File",
    }

    ctrls.VersionUP = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionUp.py", tool) ]],
        LINKS_Name = "Version UP",
        ICS_ControlPage = "File",
    }
    ctrls.VersionDOWN = {
        LINKID_DataType = "Number",
        INP_Default = 0,
        INPID_InputControl = "ButtonControl",
        BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionDown.py", tool) ]],
        LINKS_Name = "Version DOWN",
        ICS_ControlPage = "File",
    }
    return ctrls
end


function FusionHasSaverDefaults()
    for i, path in ipairs(fu:MapPathSegments("defaults:")) do
        for j, file in ipairs(bmd.readdir(path .. "*.setting")) do
            if file.Name == "Saver_Saver.setting" then
                return true
            end
        end
    end
    print("Default settings not found, updating current saver")
    return false
end


function ReconnectSavers(oldSaver, newSaver)
    print("Reconnecting Savers: ", oldSaver.Name, newSaver.Name)
    oldMainInput = oldSaver:FindMainInput(1)
    connectedToolOutput = oldMainInput:GetConnectedOutput()
    if connectedToolOutput then
        new_inp = newSaver:FindMainInput(1)
        new_inp:ConnectTo(connectedToolOutput)
    end
    oldMainOutput = oldSaver:FindMainOutput(1)
    if oldMainOutput then
        newMainOutput = newSaver:FindMainOutput(1)
        connectedInputs = oldMainOutput:GetConnectedInputs()
        for i, inp in pairs(connectedInputs) do
            inp:ConnectTo(newMainOutput)
        end
    end
    oldMainInput:ConnectTo()
end


function RenameSavers(oldSaver, newSaver)
    oldToolName = oldSaver.Name
    oldSaver:SetAttrs({TOOLB_NameSet = true, TOOLS_Name = oldToolName .. "_old"})
    newSaver:SetAttrs({TOOLB_NameSet = true, TOOLS_Name = oldToolName})
    oldSaver:SetAttrs({TOOLB_PassThrough = true})
end


function Process()
    if not tool then
        print('This is a tool script')
        return
    end
    
    if not tool.ID == "Saver" then
        print("This is not a Saver")
        return
    end

    comp:Lock()
    comp:StartUndo("Convert to SaverPlus")
    settings = comp:CopySettings(tool)

    saverPlus = comp:AddTool('Saver', -32768, -32768)
    comp:SetActiveTool(saverPlus)

    if not FusionHasSaverDefaults() then
        local ctrls = saverPlus.UserControls
        local updated_controls = UpdateUserControls(ctrls)
        saverPlus.UserControls = updated_controls
    end

    comp:SetActiveTool(saverPlus)
    RenameSavers(tool, saverPlus)
    ReconnectSavers(tool, saverPlus)
    saverPlus:Refresh()

    saverPlus = comp.ActiveTool
    saverPlus:LoadSettings(settings)
    
    comp:EndUndo()
    comp:Unlock()

end

Process()
