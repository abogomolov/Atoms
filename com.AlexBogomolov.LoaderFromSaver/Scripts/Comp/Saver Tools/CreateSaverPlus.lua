comp:Lock()

saver_plus = comp:AddTool('Saver', -32768, -32768)

local ctrls = saver_plus.UserControls

ctrls.SOLO = {
    LINKID_DataType = "Number",
    INP_Default = 0,
    INPID_InputControl = "ButtonControl",
    BTNCS_Execute = [[ comp:RunScript("Scripts:Support/SaverPlus/SoloSaver.lua") ]],
    LINKS_Name = "Solo",
    ICS_ControlPage = "File",
}

ctrls.ML = {
    LINKID_DataType = "Number",
    INP_Default = 0,
    INPID_InputControl = "ButtonControl",
    BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Comp/Saver Tools/LoaderFromSaver.lua", tool) ]],
    LINKS_Name = "Make Loader",
    ICS_ControlPage = "File",
}

ctrls.VersionUP = {
    LINKID_DataType = "Number",
    INP_Default = 0,
    INPID_InputControl = "ButtonControl",
    BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionUp.py", tool) ]],
    LINKS_Name = "Version UP",
    ICS_ControlPage = "File",
}

ctrls.VersionDOWN = {
    LINKID_DataType = "Number",
    INP_Default = 0,
    INPID_InputControl = "ButtonControl",
    BTNCS_Execute = [[ tool = comp.ActiveTool; comp:RunScript("Scripts:Support/SaverPlus/ButtonVersionDown.py", tool) ]],
    LINKS_Name = "Version DOWN",
    ICS_ControlPage = "File",
}

saver_plus.UserControls = ctrls
saver_plus:Refresh()
comp:Unlock()
