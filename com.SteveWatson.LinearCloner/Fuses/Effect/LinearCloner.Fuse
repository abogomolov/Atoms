--[[--

Linear Cloner by Steve Watson
Based on original Duplicator Fuse by Eyeon 
This is a cutdown version of Cloner for MilloLab
thanks to Bryan Ray and everyone at We Suck Less
https://www.steakunderwater.com

v0.1
ChangeLog
v0.1 
Much More limited Version of Cloner - initially for MilloLab


--]]--

FuRegisterClass("LinearCloner", CT_SourceTool, {
	REGS_Name = "LinearCloner",
	REGS_Category = "Creator",
	REGS_OpIconString = "Lcl",
	REGS_OpDescription = "Cloner of input.",
	
	REGS_Company = "SteveWatson",
	REGS_URL = "",
	
	REG_Source_GlobalCtrls = true,
	REG_Source_SizeCtrls = true,
	REG_Source_AspectCtrls = true,
	REG_Source_DepthCtrls = true,
	REG_TimeVariant = true,	-- must set this to true if Time Offset is used
	})
	
	
function Create()


	
	
	InCopies = self:AddInput("Number", "ClonesCount", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 50.0,
		INP_MinScale = 1,
		INP_Default = 2.0,
		INP_Integer    = true,
		IC_Visible = true,
		PC_Visible = true,
		})
		
	

		
	InP1 = self:AddInput("Point 1", "Point1", {
		LINKID_DataType      = "Point",
		INPID_InputControl   = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		CHC_Style 			 = "Circle",
		INP_DefaultX         = 0.25,
		INP_DefaultY         = 0.5,
		IC_Visible = true,
		PC_Visible = true,
	})
	InP2 = self:AddInput("Point 2", "Point2", {
		LINKID_DataType      = "Point",
		INPID_InputControl   = "OffsetControl",
		INPID_PreviewControl = "CrosshairControl",
		CHC_Style 			 = "Circle",
		INP_DefaultX         = 0.75,
		INP_DefaultY         = 0.5,
		IC_Visible = true,
		PC_Visible = true,
	})
	


	
	InWriteOnStart = self:AddInput("Write On Start", "WriteOnStart", {
		LINKID_DataType = "Number",
--		INP_Integer = true,
		INPID_InputControl = "RangeControl",
--		ICS_ControlPage = "Controls",
		IC_ControlGroup = 1,
--		IC_ControlID = 0,
		LINKS_Name = "Start",

		INP_MinScale = -0,
		INP_MaxScale = 1.0,
--		RNGCD_LowOuterLength = "0.1",
		INP_Default = 0,
		IC_Visible = true,
		PC_Visible = true,
	})
	
	InWriteOnEnd = self:AddInput("Write On End", "WriteOnEnd", {
		LINKID_DataType = "Number",
--		INP_Integer = true,
		INPID_InputControl = "RangeControl",
--		ICS_ControlPage = "Controls",
		IC_ControlGroup = 1,
		IC_ControlID = 1,
		LINKS_Name = "End",	
		INP_MinScale = 0,
		INP_MaxScale = 1,
--		RNGCD_HighOuterLength = "0.1",
		INP_Default = 1,
			IC_Visible = true,
		PC_Visible = true,
	})
	
	InLinearOffset = self:AddInput("Linear Offset", "LinearOffset", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		INP_Default = 0,
		INP_MaxScale = 5,
		IC_Visible = true,
		PC_Visible = true,
	})
	

	InTimeOffset = self:AddInput("Time Offset", "TimeOffset", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default 	= 0.0,
		INP_MinScale	= -5.0,
		INP_MaxScale	= 5.0,

--		IC_Visible = false,		-- If you show this input, you must set REG_TimeVariant = true
		})

	
	
	
	InAxis = self:AddInput("Axis", "Axis", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
		
		CHC_Style = "DiagonalCross",
		})

	InXSize = self:AddInput("Size", "XSize", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 5,
		INP_Default = 1.0,
		ICD_Center = 1, -- this sets the default value to the center of the slider
		})
	InYSize = self:AddInput("Y Size", "YSize", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MaxScale = 5,
		INP_Default = 1.0,
		ICD_Center = 1, 		-- this sets the default value to the center of the slider
		IC_Visible = false,		-- not yet implemented
		})

	InAngle = self:AddInput("Angle", "Angle", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
		
		INP_MinScale =   0.0,
		INP_MaxScale = 360.0,
		INP_Default = 0.0,
	
		PC_GrabPriority = 1 -- give this a higher priority than the rectangle
		})

	InXSize:SetAttrs({
		
	
		RCD_LockAspect = 1.0,
		})
		
	InApply = self:AddInput("Apply Mode", "ApplyMode", {
		LINKID_DataType = "FuID",
		INPID_InputControl = "MultiButtonControl",
	
		
		
		INP_DoNotifyChanged = true,
		{ MBTNC_AddButton = "Normal", MBTNCID_AddID = "Merge",},
		{ MBTNC_AddButton = "Screen", MBTNCID_AddID = "Screen",},
		{ MBTNC_AddButton = "Dissolve", MBTNCID_AddID = "Dissolve",},
		{ MBTNC_AddButton = "Multiply", MBTNCID_AddID = "Multiply",},
		{ MBTNC_AddButton = "Overlay",MBTNCID_AddID = "Overlay", },
		{ MBTNC_AddButton = "Soft Light", MBTNCID_AddID = "SoftLight",},
		{ MBTNC_AddButton = "Hard Light", MBTNCID_AddID = "HardLight",},
		{ MBTNC_AddButton = "Color Dodge", MBTNCID_AddID = "ColorDodge",},
		{ MBTNC_AddButton = "Color Burn", MBTNCID_AddID = "ColorBurn",},
		{ MBTNC_AddButton = "Darken", MBTNCID_AddID = "Darken",},
		{ MBTNC_AddButton = "Lighten", MBTNCID_AddID = "Lighten",},
		{ MBTNC_AddButton = "Difference", MBTNCID_AddID = "Difference",},
		{ MBTNC_AddButton = "Exclusion", MBTNCID_AddID = "Exclusion",},
		{ MBTNC_AddButton = "Hue", MBTNCID_AddID = "Hue",},
		{ MBTNC_AddButton = "Saturation", MBTNCID_AddID = "Saturation",},
		{ MBTNC_AddButton = "Color", MBTNCID_AddID = "Color",},
		{ MBTNC_AddButton = "Luminosity",  MBTNCID_AddID = "Luminosity",},
	})
		

		
	InOperation = self:AddInput("Operator", "Operator", {
		LINKID_DataType = "FuID",
		INPID_InputControl = "MultiButtonControl",
		
		{ MBTNC_AddButton = "Over", MBTNCID_AddID = "Over",},
		{ MBTNC_AddButton = "In", MBTNCID_AddID = "In",},
		{ MBTNC_AddButton = "Held Out", MBTNCID_AddID = "HeldOut",},
		{ MBTNC_AddButton = "Atop", MBTNCID_AddID = "Atop",},
		{ MBTNC_AddButton = "XOr", MBTNCID_AddID = "XOr",},
	
		
		})
		
	InAdditive = self:AddInput("Subtractive - Additive", "SubtractiveAdditive", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default = 1.0,
		SLCS_LowName = "Subtractive",
		SLCS_HighName = "Additive",
		})

	self:BeginControlNest("Gain", "GainNest", false);
		InGainRed = self:AddInput("Red Gain", "RedGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_Default = 1.0,
			})
		InGainGreen = self:AddInput("Green Gain", "GreenGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_Default = 1.0,
			})
		InGainBlue = self:AddInput("Blue Gain", "BlueGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_Default = 1.0,
			})
		InGainAlpha = self:AddInput("Alpha Gain", "AlphaGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_Default = 1.0,
			})
	self:EndControlNest()
		
	InBurn = self:AddInput("Burn In", "BurnIn", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default = 0.0,
		})
	
	InLayerBlend = self:AddInput("Blend", "LayerBlend", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_Default = 1.0,
		})




		
	self:AddControlPage("Jitter");

	InRandomise = self:AddInput("Randomize", "Randomize", {
		LINKID_DataType		= "Number",
		INPID_InputControl	= "ButtonControl",
		INP_DoNotifyChanged	= true,
		ICD_Width			= 0.25,
		})

	InSeed = self:AddInput("Random Seed", "RandomSeed", {
		LINKID_DataType		= "Number",
		INPID_InputControl	= "SliderControl",
		INP_MinAllowed,		0.0,
		INP_MaxAllowed		= 32767.0,
		INP_Integer			= true,
		ICD_Width			= 0.75,
		})


	InJitterCenter = self:AddInput("Center", "JitterCenter", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
--		INPID_PreviewControl = "CrosshairControl",
		INP_DefaultX		= 0,
		INP_DefaultY		= 0,
		})
	InJitterAxis = self:AddInput("Axis", "JitterAxis", {
		LINKID_DataType = "Point",
		INPID_InputControl = "OffsetControl",
--		INPID_PreviewControl = "CrosshairControl",
--		CHC_Style = "DiagonalCross",
		INP_DefaultX		= 0,
		INP_DefaultY		= 0,
		})

	InJitterXSize = self:AddInput("X Size", "JitterXSize", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinAllowed = 0,
		INP_MaxScale = 5,
		INP_Default = 0.0,
		ICD_Center = 1, -- this sets the default value to the center of the slider
		})

	InJitterYSize = self:AddInput("Y Size", "JitterYSize", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinAllowed = 0,
		INP_MaxScale = 5,
		INP_Default = 0.0,
		ICD_Center = 1, -- this sets the default value to the center of the slider
		IC_Visible = false,		-- not yet implemented
		})
	InJitterAngle = self:AddInput("Angle", "JitterAngle", {
		LINKID_DataType = "Number",
		INPID_InputControl = "ScrewControl",
--		INPID_PreviewControl = "AngleControl",
		INP_MinAllowed =   0.0,
		INP_MaxScale = 90.0,
		INP_Default = 0.0,
	
		PC_GrabPriority = 1 -- give this a higher priority than the rectangle
		})

	self:BeginControlNest("Gain", "JitterGainNest", false);
		InJitterGainRed = self:AddInput("Red Gain", "JitterRedGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_MinAllowed =   0.0,
			INP_Default = 0.0,
			})
		InJitterGainGreen = self:AddInput("Green Gain", "JitterGreenGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_MinAllowed =   0.0,
			INP_Default = 0.0,
			})
		InJitterGainBlue = self:AddInput("Blue Gain", "JitterBlueGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_MinAllowed =   0.0,
			INP_Default = 0.0,
			})
		InJitterGainAlpha = self:AddInput("Alpha Gain", "JitterAlphaGain", {
			LINKID_DataType = "Number",
			INPID_InputControl = "SliderControl",
			INP_MinAllowed =   0.0,
			INP_Default = 0.0,
			})
	self:EndControlNest()
	
	InJitterLayerBlend = self:AddInput("Blend", "JitterLayerBlend", {
		LINKID_DataType = "Number",
		INPID_InputControl = "SliderControl",
		INP_MinAllowed =   0.0,
		INP_Default = 0.0,
		})
	
	
	
	InBackground = self:AddInput("Clone", "Clone", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		})

	OutImage = self:AddOutput("Output", "Output", {
		LINKID_DataType = "Image",
		LINK_Main = 1,
		})
end

function NotifyChanged(inp, param, time)
	if inp ~= nil and param ~= nil then
			
		if inp == InApply and param then
				
				InOperation:SetAttrs({IC_Visible = (param.Value == "Merge" or param.Value == 0)})
		elseif inp == InRandomise then
				InSeed:SetSource(Number(math.random(0,32767)), time)
		end
	end
end

-- Alternate method of the above. Calculates only once instead of on-demand.
function calcAspect(ref_img)
	return (ref_img.Height * ref_img.YScale) / (ref_img.Width * ref_img.XScale)
end


-- Get the pixel values

function pixelValueAt(xPos,yPos,imageToCheck,imageWidth,imageHeight)
	local checkPoint = Pixel{}

	local effectorToImageRatioX =  (imageToCheck.Width/imageWidth)
	local effectorToImageRatioY =  (imageToCheck.Height/imageHeight)
	
	local normalisedX = (xPos * (imageWidth - 1)) * effectorToImageRatioX
	local normalisedY = (yPos * (imageHeight - 1)) * effectorToImageRatioY

	imageToCheck:GetPixel(normalisedX,normalisedY, checkPoint)

	return checkPoint




end

-- Do All transforms etc

function performBasicOperations(inputValue,inputBaseOperations,inputJitterOperations)
	
	local bl = math.pow(inputBaseOperations.layerBlend, inputValue)
	local rg = math.pow(inputBaseOperations.gainRed  ,  inputValue)
	local gg = math.pow(inputBaseOperations.gainGreen,  inputValue)
	local bg = math.pow(inputBaseOperations.gainBlue ,  inputValue)
	local ag = math.pow(inputBaseOperations.gainAlpha,  inputValue)
	local jaxX = inputJitterOperations.jitterAxisX   * 2 * (math.random() - 0.5)
	local jaxY = inputJitterOperations.jitterAxisY   * 2 * (math.random() - 0.5)
	local jszx = inputJitterOperations.jitterSizeX * 2 * (math.random() - 0.5)
	local jszy = inputJitterOperations.jitterSizeY * 2 * (math.random() - 0.5)
	local jan  = inputJitterOperations.jitterAngle * 2 * (math.random() - 0.5)
	local jgnr = inputJitterOperations.jitterGainRed   * 2 * (math.random() - 0.5)
	local jgng = inputJitterOperations.jitterGainGreen * 2 * (math.random() - 0.5)
	local jgnb = inputJitterOperations.jitterGainBlue  * 2 * (math.random() - 0.5)
	local jgna = inputJitterOperations.jitterGainAlpha * 2 * (math.random() - 0.5)
	local jlbl = inputJitterOperations.jitterLayerBlend * 2 * (math.random() - 0.5)
	bl = bl + jlbl
	
	
	local operationsOutput = {	jitterAxisX = jaxX,
								jitterAxisY = jaxY,
								jitterSizeX = jszx,
								jitterSizeY = jszy,
								jitterAngle = jan,
								finalRedGain = (rg * bl + jgnr),
								finalGreenGain = (gg * bl + jgng),
								finalBlueGain = (bg * bl + jgnb),
								finalAlphaGain = (ag * bl + jgna)
							}

	
	return operationsOutput

end





-- De Casteljau's equation finds x,y coordinates for a given t
-- p1 - p4 are Point DataType: Tables with indices X and Y 
-- The return value of p is a table in the same format.
function solvePoint(p1, p2, p3, p4, t)
	local p = {}
	p.X = (1-t)^3*p1.X + 3*(1-t)^2*t*p2.X + 3*(1-t)*t^2*p3.X + t^3*p4.X
	p.Y = (1-t)^3*p1.Y + 3*(1-t)^2*t*p2.Y + 3*(1-t)*t^2*p3.Y + t^3*p4.Y
	
	return p
end

-- Range Mapping Function
function map_range( a1, a2, b1, b2, s )
    return b1 + (s-a1)*(b2-b1)/(a2-a1)
end

-- Function to check if an table contains an element

function contains(table, element)

	for index, value in ipairs(table) do
		if value == element then
			return true
		end
	end
	
	return false
  
  
  
end


function Process(req) 
	-- Standard set up for Creator tools
	local realwidth = Width;
	local realheight = Height;
	
	-- We'll handle proxy ourselves
	Width = Width / Scale
	Height = Height / Scale
	Scale = 1
	
	-- Attributes for new images
	local imgattrs = {
		IMG_Document = self.Comp,
		{ IMG_Channel = "Red", },
		{ IMG_Channel = "Green", },
		{ IMG_Channel = "Blue", },
		{ IMG_Channel = "Alpha", },
		IMG_Width = Width,
		IMG_Height = Height,
		IMG_XScale = XAspect,
		IMG_YScale = YAspect,
		IMAT_OriginalWidth = realwidth,
		IMAT_OriginalHeight = realheight,
		IMG_Quality = not req:IsQuick(),
		IMG_MotionBlurQuality = not req:IsNoMotionBlur(),
		}
		
	if not req:IsStampOnly() then
		imgattrs.IMG_ProxyScale = 1
	end
	
	if SourceDepth ~= 0 then
		imgattrs.IMG_Depth = SourceDepth
	end

	-- Set up image
	local img = Image(imgattrs)
	local out = img:CopyOf()
	local p = Pixel({R=0,G=0,B=0,A=0})
	img:Fill(p) -- Clear the image so the next frame doesn't contain the previous one.
	out:Fill(p)

	local aspect = calcAspect(img)
	local img = InBackground:GetValue(req)
	

	local timeoff = InTimeOffset:GetValue(req).Value
	
	local axis = InAxis:GetValue(req)
	local xsize = InXSize:GetValue(req).Value
	local ysize = InYSize:GetValue(req).Value
	local angle = InAngle:GetValue(req).Value
	local additive = InAdditive:GetValue(req).Value
	
	
	
	local gain_red = InGainRed:GetValue(req).Value
	local gain_green = InGainGreen:GetValue(req).Value
	local gain_blue = InGainBlue:GetValue(req).Value
	local gain_alpha = InGainAlpha:GetValue(req).Value
	local burn = InBurn:GetValue(req).Value
	local apply_mode = InApply:GetValue(req).Value
	local apply_operator = InOperation:GetValue(req).Value
	
	local layerblend = InLayerBlend:GetValue(req).Value
	
	local baseOperations = {gainRed = gain_red,
							gainGreen = gain_green,
							gainBlue = gain_blue,
							gainAlpha = gain_alpha,
							layerBlend = layerblend
							}
	
		
--	local applyModes =	{  "Merge", "Screen",  "Dissolve", "Multiply","Overlay", "SoftLight", "HardLight", "ColorDodge", "ColorBurn","Darken",  "Lighten", "Difference",  "Exclusion","Hue", "Saturation", "Color", "Luminosity",  }
--	local apply_mode = applyModes[apply_mode_input]
	
	
--	local apply_operators = 	{ "Over", "In", "HeldOut","Atop", "XOr", }
--	local apply_operator = apply_operators[apply_operator_input]
	

	
	local p1  = InP1:GetValue(req)
	local p2  = InP2:GetValue(req)

	local copies = InCopies:GetValue(req).Value

	local linearOffset = InLinearOffset:GetValue(req).Value
	
	local writeOnStart 		= InWriteOnStart:GetValue(req).Value
	local writeOnEnd		= InWriteOnEnd:GetValue(req).Value
	
	
	local jitcenter = InJitterCenter:GetValue(req)
	local jitaxis = InJitterAxis:GetValue(req)
	local jitxsize = InJitterXSize:GetValue(req).Value
	local jitysize = InJitterYSize:GetValue(req).Value
	local jitangle = InJitterAngle:GetValue(req).Value
	local jitgain_red = InJitterGainRed:GetValue(req).Value
	local jitgain_green = InJitterGainGreen:GetValue(req).Value
	local jitgain_blue = InJitterGainBlue:GetValue(req).Value
	local jitgain_alpha = InJitterGainAlpha:GetValue(req).Value
	local jitlayerblend = InJitterLayerBlend:GetValue(req).Value
	
	local jitterOperations = {	jitterAxisX = jitaxis.X,
								jitterAxisY = jitaxis.Y,
								jitterSizeX = jitxsize,
								jitterSizeY = jitysize,
								jitterAngle = jitangle,
								jitterGainRed = jitgain_red,
								jitterGainGreen = jitgain_green,
								jitterGainBlue = jitgain_blue,
								jitterGainAlpha = jitgain_alpha,
								jitterLayerBlend = jitlayerblend
							}

	local seed = InSeed:GetValue(req).Value
	math.randomseed(seed)
	
	


	local i
	local cloneIndex = 0

	-- Linear Mode
	

	
	local fg 
	-- If there is just 1 copy then just put it in centre of line
	
	if copies == 1 then
		local calculatedOperations = performBasicOperations(1,baseOperations,jitterOperations)
		
			if linearOffset > 1 then
				
						linearOffset = linearOffset % 1
						
				elseif linearOffset < -1 then
				
						linearOffset = linearOffset * -1
						linearOffset = linearOffset % 1
						linearOffset = linearOffset * -1
						
				end

		offsetX = (p2.X - p1.X)/2 + p1.X + (linearOffset * (p2.X - p1.X))
		offsetY = (p2.Y - p1.Y)/2 + p1.Y + (linearOffset * (p2.Y - p1.Y))
		
		if timeoff ~= 0.0 then
							fg = InBackground:GetSource(req.Time + (timeoff ), REQF_SecondaryTime, REQF_SecondaryTime)
						else
							fg = img
						end
						
						
		if fg then
			
			out:Merge(fg, {
				MO_ApplyMode = apply_mode,
				MO_ApplyOperator = apply_operator,
				MO_XOffset = offsetX,
				MO_YOffset = offsetY,
				MO_XAxis = axis.X + calculatedOperations.jitterAxisX , 
				MO_YAxis = axis.Y + calculatedOperations.jitterAxisY ,
				MO_XSize = (math.pow(xsize, 1) * (1 + calculatedOperations.jitterSizeX)),
				MO_YSize = (math.pow(ysize, 1) * (1 + calculatedOperations.jitterSizeX)),
				MO_Angle = angle * (1) + calculatedOperations.jitterAngle,
				MO_FgAddSub = additive,
				MO_FgRedGain   = calculatedOperations.finalRedGain,
				MO_FgGreenGain = calculatedOperations.finalGreenGain, 
				MO_FgBlueGain  = calculatedOperations.finalBlueGain, 
				MO_FgAlphaGain = calculatedOperations.finalAlphaGain, 
				MO_BurnIn = burn        
				})
		end
	else 

		local xGap = ((p2.X - p1.X)*(writeOnEnd-writeOnStart))/(copies-1)
		local yGap = ((p2.Y - p1.Y)*(writeOnEnd-writeOnStart))/(copies-1)
		
		
		for i = 0, copies-1 do 
				cloneIndex = i + 1
				local calculatedOperations = performBasicOperations(i,baseOperations,jitterOperations)
				
				-- Make LinearOffset between -1 and 1
				if linearOffset > 1 then
				
						linearOffset = linearOffset % 1
						
				elseif linearOffset < -1 then
				
						linearOffset = linearOffset * -1
						linearOffset = linearOffset % 1
						linearOffset = linearOffset * -1
						
				end
				
				if linearOffset == 1 or linearOffset == -1 then 
					linearOffset = 0
				end
				
				
				offsetX = (xGap * i) + (p1.X + (writeOnStart * (p2.X - p1.X))) + (linearOffset * (p2.X - p1.X))
				offsetY = (yGap * i) + (p1.Y + (writeOnStart * (p2.Y - p1.Y))) + (linearOffset * (p2.Y - p1.Y))
				
				
				
				if not(linearOffset == 0) then 
					-- Deal with Offsets with different point1 and point2 positions
					if p2.X >= p1.X and p2.Y >= p1.Y then 
						
						if offsetX > p2.X then
							offsetX = p1.X + (((offsetX - p2.X ) ) - xGap) 
						elseif offsetX < p1.X then
							offsetX = p2.X - (((p1.X - offsetX)  ) - xGap)
						end
				
						if offsetY > p2.Y then
							offsetY = p1.Y + ((offsetY - p2.Y ) - yGap) 
						elseif offsetY < p1.Y then
							offsetY = p2.Y - ((p1.Y - offsetY)  - yGap)
						end
					elseif p2.X <= p1.X and p2.Y >= p1.Y then 
					
						if offsetX < p2.X then
							offsetX = p1.X - (((p2.X - offsetX ) ) + xGap) 
						elseif offsetX > p1.X then
							offsetX = p2.X + (((offsetX - p1.X)  ) + xGap)
						end
				
						if offsetY > p2.Y then
							offsetY = p1.Y + ((offsetY - p2.Y )  - yGap) 
						elseif offsetY < p1.Y then
							offsetY = p2.Y - ((p1.Y - offsetY) - yGap)
						end
					elseif p2.X >= p1.X and p2.Y <= p1.Y then 
						
						if offsetX > p2.X then
							offsetX = p1.X + (((offsetX - p2.X ) ) - xGap) 
						elseif offsetX < p1.X then
							offsetX = p2.X - (((p1.X - offsetX)  ) - xGap)
						end
				
						if offsetY > p1.Y then
							offsetY = p2.Y + ((offsetY - p1.Y ) + yGap) 
						elseif offsetY < p2.Y then
						
							offsetY = p1.Y - ((p2.Y - offsetY)  + yGap)
						end
					elseif p2.X <= p1.X and p2.Y <= p1.Y then
						
						if offsetX < p2.X then
							offsetX = p1.X - (((p2.X - offsetX ) ) + xGap) 
						elseif offsetX > p1.X then
							offsetX = p2.X + (((offsetX - p1.X)  ) + xGap)
						end 
					
					
						if offsetY > p1.Y then
							offsetY = p2.Y + ((offsetY - p1.Y ) + yGap) 
						elseif offsetY < p2.Y then
						
							offsetY = p1.Y - ((p2.Y - offsetY)  + yGap)
						end
					end
				end
				
						if timeoff ~= 0.0 then
							fg = InBackground:GetSource(req.Time + (timeoff ), REQF_SecondaryTime, REQF_SecondaryTime)
						else
							fg = img
						end

				if fg then
	
						
					 
						out:Merge(fg, {
							MO_ApplyMode = apply_mode,
							MO_ApplyOperator = apply_operator,
							MO_XOffset = offsetX,
							MO_YOffset = offsetY,
							MO_XAxis = axis.X + calculatedOperations.jitterAxisX, 
							MO_YAxis = axis.Y + calculatedOperations.jitterAxisY,
							MO_XSize = (math.pow(xsize, i) * (1 + calculatedOperations.jitterSizeX)),
							MO_YSize = (math.pow(ysize, i) * (1 + calculatedOperations.jitterSizeX)),
							MO_Angle = angle * (i) + calculatedOperations.jitterAngle,
							MO_FgAddSub = additive,
							MO_FgRedGain   = calculatedOperations.finalRedGain,
							MO_FgGreenGain = calculatedOperations.finalGreenGain,
							MO_FgBlueGain  = calculatedOperations.finalBlueGain,
							MO_FgAlphaGain = calculatedOperations.finalAlphaGain,
							MO_BurnIn = burn         
							})
						
				end


		end
		
	end
		

	

	if TimeExtent and timeoff ~= 0 and copies > 1 then
		local te = TimeExtent({req.Time, req.Time})	-- prevents static caching
		req:SetOutputData(OutImage, out, te)
	else
		OutImage:Set(req, out)
	end
end