# Vonk Ultra - JSON

JSON is a node based JSON library for Blackmagic Design Fusion.

## Acknowledgements

- Kristof Indeherberge
- Cédric Duriau

## Contents

**Fuses**

- `vJSONFromFile.fuse`: Fuse to read a JSON string from a file.
- `vJSONGet.fuse`: Fuse to get the value of a key in a JSON table.
- `vJSONSet.fuse`: Fuse to set a key value pair in a JSON table.
- `vJSONToFile.fuse`: Fuse to write a JSON string into a file.

**Modules/Lua**

- `jsonutils.lua`: Core module for JSON operations.
