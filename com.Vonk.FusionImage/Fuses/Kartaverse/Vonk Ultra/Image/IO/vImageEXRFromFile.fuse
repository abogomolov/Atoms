-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageEXRFromFile"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads an EXR image from a file.",
    REGS_OpIconString = FUSE_NAME,

    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})


function Create()
    InFilename = self:AddInput('Filename', 'Filename', {
        LINKID_DataType = 'Text',
        INPID_InputControl = "TextEditControl",
        INP_Passive = true,
        LINK_ForceSave = true,
        TEC_Lines = 1,
        LINK_Main = 1,
    })

--  InFilename = self:AddInput('Filename', 'Filename', {
--      LINKID_DataType = 'Text',
--      INPID_InputControl = 'FileControl',
--      FC_IsSaver = true,
--      FC_ClipBrowse = true,
--      FCS_FilterString = 'OpenEXR Files (*.exr)|*.exr|',
--      INP_Passive = true,
--      LINK_ForceSave = true,
--      LINK_Main = 1,
--  })

    InPart = self:AddInput("EXR Part Number", "Part", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 1,
        INP_MaxScale = 1000,
        INP_MinAllowed = 1,
        INP_Default = 1,
        INP_Integer = true,
        -- LINK_Main = 2,
    })

    InTimeMode = self:AddInput("Time Mode", "TimeMode", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ComboControl",
        INP_Default = 0,
        INP_Integer = true,
        ICD_Width = 1,
        {CCS_AddString = "Static Frame"},
        {CCS_AddString = "Current Frame"},
        {CCS_AddString = "Request Time"},
        {CCS_AddString = "Time"},
        CC_LabelPosition = "Vertical",
        INP_DoNotifyChanged = true
    })

    InTime = self:AddInput("Time", "Time", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = 0.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutImage = self:AddOutput('Output', 'Output', {
        LINKID_DataType = 'Image',
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFilename:SetAttrs({LINK_Visible = visible})
        InTime:SetAttrs({LINK_Visible = visible})
    elseif inp == InTimeMode then
        local visible
        if param.Value == 3.0 then visible = true else visible = false end

        InTime:SetAttrs({IC_Visible = visible})
    end
end

function Process(req)
    local rel_path = InFilename:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)
    local filename = abs_path

    local part_number = InPart:GetValue(req).Value
    local out

    local t = InTime:GetValue(req).Value

    if bmd.fileexists(filename) then
        local frame

        local time_mode = InTimeMode:GetValue(req).Value
        if time_mode == 0 then
            -- Static Frame
            frame = -1
        elseif time_mode == 1 then
             -- Current Frame
            frame = self.Comp.CurrentTime
        elseif time_mode == 2 then
            -- Request Time
            frame = req.Time
        elseif time_mode == 3 then
            -- Time Input
            frame = tonumber(t)
        end

        if frame == nil then
          frame = -1
        end

        local exr = EXRIO()
        -- exr:ReadOpen(self.Comp:MapPath(filename), req.Time)
        -- exr:ReadOpen(self.Comp:MapPath(filename), -1)
        exr:ReadOpen(filename, frame)

        if exr:ReadHeader() then
            dispw = exr:DisplayWindow(1)
            dataw = exr:DataWindow(1)
            local ox, oy = dispw.left, dispw.bottom
            local w, h = dispw.right - dispw.left, dispw.top - dispw.bottom


            local imgw = ImgRectI(dataw)
            imgw:Offset(-ox, -oy)

            out = Image({
                IMG_Width = w,
                IMG_Height = h,
                IMG_Depth = IMDP_128bitFloat,
                IMG_DataWindow = imgw,
                IMG_NoData = req:IsPreCalc(),
                IMG_YScale = 1.0/exr:PixelAspectRatio(part_number),
            })

            if not req:IsPreCalc() then
                exr:Part(part_number)

                exr:Channel("R", ANY_TYPE, 1, CHAN_RED)
                exr:Channel("G", ANY_TYPE, 1, CHAN_GREEN)
                exr:Channel("B", ANY_TYPE, 1, CHAN_BLUE)
                -- exr:Channel("A", ANY_TYPE, 1, CHAN_ALPHA, 1.0)

                exr:ReadPart(part_number, {out})

                -- Part Attributes
                local metadata = {}
                for k,v in pairs(exr:GetAttributeNames(part_number)) do
                    -- Select the 2nd value returned from GetAttribute which is the metadata entry
                    metadata[v] = select(2,exr:GetAttribute(part_number, v))
                end
                table.sort(metadata)
                out.Metadata = metadata
            end
        end

        exr:Close()

        err = exr:GetLastError()
        if #err > 0 then
            print(bmd.writestring(err))
        end
    else
        error(string.format("[EXRIO] Image '%s' missing from disk at frame %d.", filename, req.Time))
    end

    OutImage:Set(req, out)
end
