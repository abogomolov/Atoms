-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageFromZip"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Reads an Image object from a zip archive.",
    REGS_OpIconString = FUSE_NAME,

    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})

function Create()
    -- [[ Creates the user interface. ]]
    InZipFile = self:AddInput("Zip File", "ZipFile", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = false,
        FC_ClipBrowse = false,
        FCS_FilterString =  "Zip (*.zip)|*.zip",
        LINK_Main = 1
    })

    InExtractImage = self:AddInput("Extract Image", "ExtractImage", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutData = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Image",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InZipFile:SetAttrs({LINK_Visible = visible})
        InExtractImage:SetAttrs({LINK_Visible = visible})
    end
end


-- Unzip a file and return it as a string filled with binary data
function UnzipReadFile(zipFilename, extractFilename)
    if zipFilename == "" then
        error("[Zip] [Zip File] Empty filepath string")
    elseif extractFilename == "" then
        error("[Zip] [Extract File] Empty filepath string")
    end

    -- Create a file handler for the fuskin zipfile
    local zip = ZipFile(zipFilename, false)

    -- Verify the zip file could be accessed
    if zip:IsOpen() then
        -- print("[Zip] [Opened Zip] ", zipFilename)

        -- Search for a file
        if zip:LocateFile(extractFilename, true) then
            -- print("[Zip] [Found] ", extractFilename)

            -- Access the specific image
            if zip:OpenFile() then
                -- print("[Zip]\t[Opened File] ", tostring(zip:GetFileName()))

                -- Print the file creation date as YYYY-MM-DD
                -- print("[Zip]\t[File Date] ", os.date("%Y-%m-%d", zip:GetFileTime()))

                filesize = tonumber(zip:GetFileSize())
                -- print("[Zip]\t[File Size] ", tostring(filesize))

                -- Create an FFI based buffer variable to hold the extracted file data
                -- Note: You might have to increase the 'BUF_SIZE' value to allow larger then 65K files to be extracted in your scripts.
                local BUF_SIZE = 65536
                local buf = ffi.new("char[?]", BUF_SIZE)

                -- Read the file from the zip
                -- The 'zip:readfile()' size argument should be set to the value returned by 'zip:GetFileSize()'
                local len = zip:ReadFile(buf, filesize)
                -- print("[Zip]\t[Bytes Read] ", tostring(len))

                -- Close the zip
                zip:CloseFile()

                if len > 0 then
                    -- print("[Zip]\t[Extracted File Sucessfully] ")

                    -- return the binary data that was extracted
                    return ffi.string(buf, len)
                else
                    error(string.format("[Zip]\t[Extracted File is Empty]"))
                    return nil
                end
            else
                error(string.format("[Zip]\t[Unable to Open File] %s", extractFilename))
                return nil
            end
        else
            error(string.format("[Zip]\t[Not Found] %s", extractFilename))
            return nil
        end
    else
        error(string.format("[Zip] [Unable to Open Zip] %s", zipFilename))
        return nil
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local rel_path = InZipFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local extract = InExtractImage:GetValue(req).Value
    local ext = string.match(extract, "%.(.+)$")
    local zip_str = UnzipReadFile(abs_path, extract)

    local nodeName = self.Name
    local folder = self.Comp:MapPath("Temp:/Vonk/")
    local path = self.Comp:MapPath(folder .. tostring(nodeName)  .. "_" .. tostring(bmd.createuuid()) .. "." .. tostring(ext))
    if bmd.fileexists(folder) == false then
        bmd.createdir(folder)
    end

    -- Save file to disk
    local file = io.open(path, "wb")
    if file then
        file:write(zip_str)
        file:close()
    else
        error(string.format("[Zip] [Temp Image Write Error] %s", destFilename))
    end

    -- Verify the file exists
    if bmd.fileexists(path) == true then
        -- Get the image clip
        
        -- Fusion v9 - 16.0.0
        -- local clip = Clip(filename)
        -- out = clip:GetFrame(0)

        -- Fusion 16.1+
        local clip = Clip(path, false)
        clip:Open()
        img = clip:GetFrame(0)
        clip:Close()

        -- Create output image
        local tmpImg = Image({
            IMG_Like = img,
        })
        img:Crop(tmpImg, {})

        -- Grab the Metadata Lua table from the image input
        local meta = tmpImg.Metadata or {}

        -- Define the Filename metadata attribute
        meta.Filename = path

        -- Push the revised metadata back into the table
        tmpImg.Metadata = meta

        -- Push the result to the node's output connection
        OutData:Set(req, tmpImg)
    else
        -- Fallback to a blank canvas when no image is found
        local compWidth = self.Comp:GetPrefs("Comp.FrameFormat.Width") or 1920
        local compHeight = self.Comp:GetPrefs("Comp.FrameFormat.Height") or 1080

        emptyImage = Image({
            IMG_Width = compWidth,
            IMG_Height = compHeight,
            })

        -- Pixel defaults to black/clear
        emptyImage:Fill(Pixel())

        -- Push the empty result to the node's output connection
        OutData:Set(req, emptyImage)
    end
end
