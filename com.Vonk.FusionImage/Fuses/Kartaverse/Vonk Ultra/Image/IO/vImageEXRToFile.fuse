
-- ============================================================================
-- modules
-- ============================================================================

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vImageEXRToFile"
DATATYPE = "Image"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Image\\IO",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Saves an EXR image to disk.",
    REGS_OpIconString = FUSE_NAME,
    
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        LINK_Main = 1,
    })

    InFilename = self:AddInput('Filename', 'Filename', {
        LINKID_DataType = 'Text',
        INPID_InputControl = "TextEditControl",
        INP_Passive = true,
        LINK_ForceSave = true,
        TEC_Lines = 1,
        LINK_Main = 2,
    })

--  InFilename = self:AddInput('Filename', 'Filename', {
--      LINKID_DataType = 'Text',
--      INPID_InputControl = 'FileControl',
--      FC_IsSaver = true,
--      FC_ClipBrowse = true,
--      FCS_FilterString = 'OpenEXR Files (*.exr)|*.exr|',
--      INP_Passive = true,
--      LINK_ForceSave = true,
--      LINK_Main = 2,
--  })

    InCompression = self:AddInput('Compression', 'Compression', {
        INPID_InputControl = 'ComboControl',
        INP_DoNotifyChanged = true,
        INP_External = false,
        INP_Default = 2,
        INP_Integer = true,
        ICS_Name = 'Compression',
        ICD_Width = 1,
        {CCS_AddString = 'None'},
        {CCS_AddString = 'RLE'},
        {CCS_AddString = 'ZIP'},
        {CCS_AddString = 'ZIP Scanline'},
        {CCS_AddString = 'Piz (wavelet)'},
        {CCS_AddString = 'Pxr24'},
        {CCS_AddString = 'B44'},
        {CCS_AddString = 'B44A'},
        {CCS_AddString = 'DWA'},
        {CCS_AddString = 'DWAB'},
        CC_LabelPosition = 'Horizontal',
        LINK_ForceSave = true,
    })

    OutImage = self:AddOutput('Output', 'Output', {
        LINKID_DataType = 'Image',
        LINK_Main = 1,
    })
end


function Process(req)
    local img = InImage:GetValue(req)
    local filename = InFilename:GetValue(req).Value
    local abs_filename = self.Comp:MapPath(filename)

    -- Get the 'Compression Layer #' settings
    local compression = InCompression:GetValue(req).Value
    local compressionName = ""

    --local depth = HALF
    local depth = FLOAT

    local exr = EXRIO()

    if abs_filename and abs_filename ~= '' then
        if not req:IsPreCalc() then
            --exr:WriteOpen(abs_filename, req.Time + 1)
            exr:WriteOpen(abs_filename, -1)

            local p = exr:Part("Image", img.ImageWindow, img.ImageWindow, img.XScale/img.YScale)

            exr:Channel("R", depth, img, CHAN_RED)
            exr:Channel("G", depth, img, CHAN_GREEN)
            exr:Channel("B", depth, img, CHAN_BLUE)
            exr:Channel("A", depth, img, CHAN_ALPHA, 1.0)

            -- Compression
            if compression == 0 then
                -- None
                compressionName = "None"
                exr:SetAttribute(p, 'compression', 'compression', NO_COMPRESSION)
            elseif compression == 1 then
                -- RLE
                compressionName = "RLE"
                exr:SetAttribute(p, 'compression', 'compression', RLE_COMPRESSION)
            elseif compression == 2 then
                -- ZIP
                compressionName = "ZIP"
                exr:SetAttribute(p, 'compression', 'compression', ZIP_COMPRESSION)
            elseif compression == 3 then
                -- ZIP Scanline
                compressionName = "ZIP Scanline"
                exr:SetAttribute(p, 'compression', 'compression', ZIPS_COMPRESSION)
            elseif compression == 4 then
                -- Piz (wavelet)
                compressionName = "Piz (wavelet)"
                exr:SetAttribute(p, 'compression', 'compression', PIZ_COMPRESSION)
            elseif compression == 5 then
                -- Pxr24
                compressionName = "Pxr24"
                exr:SetAttribute(p, 'compression', 'compression', PXR24_COMPRESSION)
            elseif compression == 6 then
                -- B44
                compressionName = "B44"
                exr:SetAttribute(p, 'compression', 'compression', B44_COMPRESSION)
            elseif compression == 7 then
                -- B44A
                compressionName = "B44A"
                exr:SetAttribute(p, 'compression', 'compression', B44A_COMPRESSION)
            elseif compression == 8 then
                -- DWA
                compressionName = "DWA"
                exr:SetAttribute(p, 'compression', 'compression', DWAA_COMPRESSION)
            elseif compression == 9 then
                -- DWAB
                compressionName = "DWAB"
                exr:SetAttribute(p, 'compression', 'compression', DWAB_COMPRESSION)
            else
                -- Zip fallback
                exr:SetAttribute(p, 'compression', 'compression', ZIP_COMPRESSION)
            end

            if exr:WriteHeader() then
                exr:WritePart(p, { })
            end

            exr:Close()

            local err = exr:GetLastError()
            if #err > 0 then
                print(bmd.writestring(err))
            end
        end
    else
        error('[EXRIO] [Error] The filename field is empty!')
    end

    OutImage:Set(req, img)
end
