Overview 
This atom package provides the vcredist (Microsoft Visual C++ redistributable library) installers needed to use the Reactor BIN category atoms on a freshly reloaded Windows bare-metal system. Microsoft vc-redist files are needed to run programs that were compiled using Microsoft's Visual Studio development environment. 
Also included are the Microsoft .NET v3.5 and v4.5 framework installers. 
More Info
https://support.microsoft.com/en-us/help/2977003/the-latest-supported-visual-c-downloads 
Microsoft .NET Development Center
https://dotnet.microsoft.com/ 
Microsoft .NET Docs
https://docs.microsoft.com/en-us/dotnet/framework/ 
Windows Installer 3.1
https://support.microsoft.com/en-gb/help/893803/windows-installer-3-1-v2-3-1-4000-2435-is-available
