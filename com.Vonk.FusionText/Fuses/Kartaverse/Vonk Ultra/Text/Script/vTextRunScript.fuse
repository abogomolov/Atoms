-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextRunScript"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_SinkTool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Run an external Lua or Python script.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]

    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 1,
        -- TEC_Wrap = true,
        INP_Passive = true,
        LINK_Main = 1
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        InFile:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local file = InFile:GetValue(req).Value
    local abs_file = self.Comp:MapPath(file)
    if bmd.fileexists(abs_file) then
        dofile(abs_file)
    else
        error(string.format("[RunScript] File not found '%s'"), file)
    end

    local out = file
    OutText:Set(req, Text(out))
end
