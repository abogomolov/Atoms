-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextViewer"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Utility",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "View the Fusion text object contents in the Inspector.",
    REGS_OpIconString = FUSE_NAME,
    -- Should the current time setting be cached?
    REG_TimeVariant = true,
    REG_Unpredictable = true,
    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand",
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Input" , "Input" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        IC_NoLabel = true,
        TEC_Lines = 25,
        TEC_FontSize = 18,
        -- TEC_Wrap = true,
        INP_Passive = true,
        LINK_Main = 1,
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InUISeparator1 = self:AddInput("UISeparator1", "UISeparator1", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    InFontSize = self:AddInput("Font Size", "FontSize", {
        LINKID_DataType      = "Number",
        INPID_InputControl   = "MultiButtonControl",
        INP_Integer          = true,
        INP_Default          = 3.0,
        MBTNC_StretchToFit = true,
        {MBTNC_AddButton     = "12"},
        {MBTNC_AddButton     = "14"},
        {MBTNC_AddButton     = "16"},
        {MBTNC_AddButton     = "18"},
        {MBTNC_AddButton     = "24"},
        {MBTNC_AddButton     = "36"},
        {MBTNC_AddButton     = "48"},
        {MBTNC_AddButton     = "72"},
        INP_DoNotifyChanged  = true,
        INP_Passive = true,
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_Passive = true,
        INP_DoNotifyChanged = true
    })
    
    InUISeparator2 = self:AddInput("UISeparator2", "UISeparator2", {
        IC_Visible = true,
        INPID_InputControl = "SeparatorControl",
        INP_External = false,
    })

    OutText = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InFontSize then
        -- Change the TextEditControl font size in points
        local fontSizeTbl = {12, 14, 16, 18, 24, 36, 48, 72}
        local fontSizeSelect = fontSizeTbl[param.Value + 1]
        InText:SetAttrs({TEC_FontSize = fontSizeSelect})
        -- print("[Font Size Pt]", fontSizeSelect)

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local txt_str = InText:GetValue(req).Value
    local show_dump = InShowDump:GetValue(req).Value

    if show_dump == 1 then
        print("\n----------------------")
        print("[" .. tostring(self.Name) .. "]")
        dump(txt_str)
    end

    local out = Text(txt_str)

    OutText:Set(req, out)
end
