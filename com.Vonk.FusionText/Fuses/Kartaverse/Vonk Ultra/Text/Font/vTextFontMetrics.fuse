-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vTextFontMetrics"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Text\\Font",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Return font measurements as Fusion Number objects.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
    REGS_IconID = "Icons.Tools.Icons.TextPlus"
})

function Create()
    -- [[ Creates the user interface. ]]
    InFont = self:AddInput("Font", "Font", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FontFileControl",
        IC_ControlGroup = 2,
        IC_ControlID = 0,
        INP_Level = 1,
        INP_DoNotifyChanged = true,
    })

    InFontStyle = self:AddInput("Style", "Style", {
        LINKID_DataType = "Text",
        INPID_InputControl = "FontFileControl",
        IC_ControlGroup = 2,
        IC_ControlID = 1,
        INP_Level = 1,
        INP_DoNotifyChanged = true,
    })

    InText = self:AddInput( "Styled Text", "StyledText", {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 10,
    })

    InFontSize = self:AddInput("Size", "Size", {
        LINKID_DataType = "Number",
        INPID_InputControl = "SliderControl",
        INP_MinScale =    0.0,
        INP_MaxScale =    0.5,
        INP_Default =     0.1,
    })

    OutFont = self:AddOutput("Font", "Font", {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })

    OutFontStyle = self:AddOutput("Style", "Style", {
        LINKID_DataType = "Number",
        LINK_Main = 2
    })

    OutFontSize = self:AddOutput("Size", "Size", {
        LINKID_DataType = "Number",
        LINK_Main = 3
    })

    OutTextAscent = self:AddOutput("Ascent", "Ascent", {
        LINKID_DataType = "Number",
        LINK_Main = 4
    })

    OutTextDescent = self:AddOutput("Descent", "Descent", {
        LINKID_DataType = "Number",
        LINK_Main = 5
    })

    OutTextExternalLeading = self:AddOutput("ExternalLeading", "ExternalLeading", {
        LINKID_DataType = "Number",
        LINK_Main = 6
    })
end


function NotifyChanged(inp, param, time)
    -- when the tools FontFileControl is first created, the FontManager has not yet
    -- provided a FontList, so we can't set a default value. Instead we do it here.

    if inp == InFont then
        local f = param.Value

        if f == nil or string.len(f) == 0 then
            InFont:SetSource(Text("Arial"), time)
        end
    elseif inp == InFontStyle then
        local f = param.Value

        if f == nil or string.len(f) == 0 then
            InFontStyle:SetSource(Text("Regular"), time)
        end
    end

    -- ---------------------

    if inp == InPosition then   -- Center moved, update rectangle control position
        InWrap_Height:SetAttrs({ RCD_SetX = param.X, RCD_SetY = param.Y })
    elseif inp == InOperation then
        InWrap_Height:SetAttrs({ PC_Visible = (param.Value < 1) })
        InPosition:SetAttrs({ PC_Visible = (param.Value < 1) })
    end
end


function Process(req)
    -- [[ Creates the output. ]]
    
    -- if the FontManager list is empty, scan the font list
    -- If the UI has never been shown, as would always be the case on a render slave,
    -- nothing will scan the font list for available fonts. So we check for that here,
    -- and force a scan if needed.
    if not next(FontManager:GetFontList()) then
        FontManager:ScanDir()
    end

    local font = InFont:GetValue(req).Value
    dump(font)

    local style = InFontStyle:GetValue(req).Value
    dump(style)

    local text = InText:GetValue(req).Value
    local size = InFontSize:GetValue(req).Value

    --local font = TextStyleFont("NSimSun", "Regular")
    local font = TextStyleFont(font , style)
    local tfm = TextStyleFontMetrics(font)

    local out = Number(self.Comp.CurrentTime)
    --dump(tfm.TextAscent)
    --dump(tfm.TextDescent)
    --dump(tfm.TextExternalLeading)

    --local line_height = (tfm.TextAscent + tfm.TextDescent + tfm.TextExternalLeading) * 10 * size +line_spacing
    --local line_height = (tfm.TextAscent + tfm.TextDescent + tfm.TextExternalLeading) * 10 * 0.08 + 1.0
    --dump(line_height)

    dump("CharWidthAverage: ", tfm.CharWidthAverage)
    dump("CharWidthMax: ", tfm.CharWidthMax)
    dump("CharWidthSpace: ", tfm.CharWidthSpace)
    --dump("DoStrikeout: ", tfm.DoStrikeout)
    --dump("DoUnderline: ", tfm.DoUnderline)
    dump("FontSize: ", tfm.FontSize)
    dump("Scale: ", tfm.Scale)
    dump("TextAscent: ", tfm.TextAscent)
    dump("TextDescent: ", tfm.TextDescent)
    dump("TextExternalLeading: ", tfm.TextExternalLeading)
    dump("TextInternalLeading: ", tfm.TextInternalLeading)
    dump("TextOverhang: ", tfm.TextOverhang)
    dump("TypeName: ", tfm.TypeName)
    dump("TypeNamePtr: ", tfm.TypeNamePtr)
    dump("UnderlinePosition: ", tfm.UnderlinePosition)
    dump("UnderlineThickness: ", tfm.UnderlineThickness)
    
    -- CalcCharacterWidth()
    -- float64 TextStyleFontMetrics:CalcCharacterWidth(int32 ch)
    -- CharacterKerning()
    -- float64 TextStyleFontMetrics:CharacterKerning(int32 first, int32 second)
    -- CharacterWidth()
    -- float64 TextStyleFontMetrics:CharacterWidth(int32 ch)
    -- GetError()
    -- uint32 TextStyleFontMetrics:GetError()
    -- TextStyleFontMetrics()
    -- TextStyleFontMetrics constructor
    -- TextStyleFontMetrics TextStyleFontMetrics(TextStyleFont font)
    
    --* 10 * size +line_spacing
    --Output:Set(req, out)
    OutFont:Set(req, Text(font))
    OutFontStyle:Set(req, Text(style))
    OutFontSize:Set(req, Number(size))
    OutTextAscent:Set(req, Number(tfm.TextAscent))
    OutTextDescent:Set(req, Number(tfm.TextDescent))
    OutTextExternalLeading:Set(req, Number(tfm.TextExternalLeading))

    --Output:Set(req, Number(23))
end
