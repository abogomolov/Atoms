--[[-- 
Copyright (c) 2020,  Emilio Sapia
https://emiliosapia.myportfolio.com
Written by : Emilio Sapia 
Written on : Aug, 2020
version 1.0

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
--]]--

{
	Tools = ordered() {
		ml_LFGlow = MacroOperator {
			CtrlWZoom = false,
			Inputs = ordered() {
				Input = InstanceInput {
					SourceOp = "GLOW",
					Source = "Background",
				},
				Controls = InstanceInput {
					SourceOp = "GLOW",
					Source = "Controls",
					Default = 0,
				},
				Blank1 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				Gain = InstanceInput {
					SourceOp = "BrightnessContrast2",
					Source = "Gain",
					Name = "Brightness",
					Default = 1,
				},
				Gamma = InstanceInput {
					SourceOp = "BrightnessContrast2",
					Source = "Gamma",
					Name = "Size",
					Default = 2,
				},
				Aspect1 = InstanceInput {
					SourceOp = "Transform1",
					Source = "Aspect",
					Default = 1,
				},
				Blend = InstanceInput {
					SourceOp = "GLOW",
					Source = "Blend",
					Name = "Main Blend",
					Default = 1,
				},
				GainNest = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainNest",
					Name = "Core Color",
				},
				GainRed = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainRed",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GainGreen = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainGreen",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GainBlue = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainBlue",
					ControlGroup = 1,
					Page = "Controls",
					Default = 0.9,
				},
				GainAlpha = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GainAlpha",
					ControlGroup = 1,
					Page = "Controls",
					Default = 1,
				},
				GammaNest = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GammaNest",
					Name = "Main Color",
				},
				GammaRed = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GammaRed",
					ControlGroup = 2,
					Page = "Controls",
					Default = 0.9,
				},
				GammaGreen = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GammaGreen",
					ControlGroup = 2,
					Page = "Controls",
					Default = 0.9,
				},
				GammaBlue = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GammaBlue",
					ControlGroup = 2,
					Page = "Controls",
					Default = 1,
				},
				InpuGammaAlphat8 = InstanceInput {
					SourceOp = "ColorGain1",
					Source = "GammaAlpha",
					ControlGroup = 2,
					Page = "Controls",
					Default = 1,
				},
				SecondarySize = InstanceInput {
					SourceOp = "Down",
					Source = "SizeCopy",
					Name = "Secondary Size",
					Page = "Secondary",
					Default = 4,
				},
				Aspect2 = InstanceInput {
					SourceOp = "Transform2",
					Source = "Aspect",
					Name = "Secondary Aspect",
					Default = 1,
				},
				SecondaryBlend = InstanceInput {
					SourceOp = "Down",
					Source = "Blend",
					Name = "Secondary Blend",
					Page = "Secondary",
					Default = 0.12,
				},
				GammaNest2 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GammaNest",
					Name = "Secondary Color",
					Page = "Secondary",
				},
				GainRed2 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GainRed",
					ControlGroup = 5,
					Page = "Secondary",
					Default = 0.9,
				},
				GainGreen2 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GainGreen",
					ControlGroup = 5,
					Page = "Secondary",
					Default = 0.8,
				},
				GainBlue2 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GainBlue",
					ControlGroup = 5,
					Page = "Secondary",
					Default = 1,
				},
				GainAlpha2 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GainAlpha",
					ControlGroup = 5,
					Page = "Secondary",
					Default = 1,
				},
				SparkleSize = InstanceInput {
					SourceOp = "Merge2",
					Source = "SizeCopy",
					Name = "Sparkle Size",
					Page = "Sparkle",
					Default = 2.5,
				},
				Aspect3 = InstanceInput {
					SourceOp = "Transform3",
					Source = "Aspect",
					Name = "Sparkle Aspect",
					Default = 1,
				},
				SparkleBlend = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blend",
					Name = "Sparkle Blend",
					Page = "Sparkle",
					Default = 0.4,
				},
				Length = InstanceInput {
					SourceOp = "DirectionalBlur1",
					Source = "Length",
					Default = 0,
				},
				GainNest3 = InstanceInput {
					SourceOp = "ColorGain2",
					Source = "GammaNest",
					Name = "Sparkle Color",
					Page = "Sparkle",
				},
				Input15 = InstanceInput {
					SourceOp = "ColorGain3",
					Source = "GainRed",
					ControlGroup = 6,
					Page = "Sparkle",
					Default = 1,
				},
				Input16 = InstanceInput {
					SourceOp = "ColorGain3",
					Source = "GainGreen",
					ControlGroup = 6,
					Page = "Sparkle",
					Default = 1,
				},
				Input17 = InstanceInput {
					SourceOp = "ColorGain3",
					Source = "GainBlue",
					ControlGroup = 6,
					Page = "Sparkle",
					Default = 0.85,
				},
				Input18 = InstanceInput {
					SourceOp = "ColorGain3",
					Source = "GainAlpha",
					ControlGroup = 6,
					Page = "Sparkle",
					Default = 1,
				},
				Nest = InstanceInput {
					SourceOp = "FN",
					Source = "NoiseNest",
					Page = "Sparkle",
				},
				Detail = InstanceInput {
					SourceOp = "FN",
					Source = "Detail",
					Default = 10,
				},
				Brightness = InstanceInput {
					SourceOp = "FN",
					Source = "Brightness",
					Default = 0,
				},
				Contrast = InstanceInput {
					SourceOp = "FN",
					Source = "Contrast",
					Default = 2.77,
				},
				XScale = InstanceInput {
					SourceOp = "FN",
					Source = "XScale",
					Default = 8.54,
				},
				YScale = InstanceInput {
					SourceOp = "FN",
					Source = "YScale",
					Default = 0,
				},
				Seethe = InstanceInput {
					SourceOp = "FN",
					Source = "Seethe",
					Default = 0.25,
				},
				SeetheRate = InstanceInput {
					SourceOp = "FN",
					Source = "SeetheRate",
					Default = 0.15,
				},
				Blank3 = InstanceInput {
					SourceOp = "Merge2",
					Source = "Blank1",
				},
				Discontinuous = InstanceInput {
					SourceOp = "FN",
					Source = "Discontinuous",
					Default = 0,
				},
				Inverted = InstanceInput {
					SourceOp = "FN",
					Source = "Inverted",
					Default = 0,
				}
			},
			Outputs = {
				MainOutput1 = InstanceOutput {
					SourceOp = "SetDomain2",
					Source = "Output",
				}
			},
			ViewInfo = GroupInfo {
				Pos = { 1234.67, 150.394 },
				Flags = {
					Expanded = true,
					AllowPan = false,
					ForceModes = false,
					ConnectedSnap = true,
					AutoSnap = true,
					ShowInstance = false,
					Thumbnails = false,
					RemoveRouters = true
				},
				Size = { 963.765, 426.491, 481.883, 24.2424 },
				Direction = "Vertical",
				PipeStyle = "Direct",
				Scale = 1,
				Offset = { 0, 0 }
			},
			Tools = ordered() {
				ColorCurves1 = ColorCurves {
					PickColor = true,
					CtrlWShown = false,
					Inputs = {
						Red = Input {
							SourceOp = "ColorCurves1Red",
							Source = "Value",
						},
						Green = Input {
							SourceOp = "ColorCurves1Green",
							Source = "Value",
						},
						Blue = Input {
							SourceOp = "ColorCurves1Blue",
							Source = "Value",
						},
						Alpha = Input {
							SourceOp = "ColorCurves1Alpha",
							Source = "Value",
						},
						NumberOfSamplesOnMatchCurve = Input { Value = 64, },
						IgnoreTransparent = Input { Value = 1, },
						Input = Input {
							SourceOp = "CineonLog1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -241.656, 55.653 } },
					Tools = {
						ColorCurves1Blue = LUTBezier {
							KeyColorSplines = {
								[0] = {
									[0] = { 0, RH = { 0.670231729055258, 0.00740740740740729 }, Flags = { Linear = true } },
									[1] = { 1, LH = { 0.75222816399287, 0.444444444444445 } }
								}
							},
							SplineColor = { Red = 68, Green = 68, Blue = 255 },
							NameSet = true,
						},
						ColorCurves1Green = LUTBezier {
							KeyColorSplines = {
								[0] = {
									[0] = { 0, RH = { 0.670231729055258, 0.00740740740740729 }, Flags = { Linear = true } },
									[1] = { 1, LH = { 0.75222816399287, 0.444444444444445 } }
								}
							},
							SplineColor = { Red = 0, Green = 255, Blue = 0 },
							NameSet = true,
						},
						ColorCurves1Alpha = LUTBezier {
							KeyColorSplines = {
								[0] = {
									[0] = { 0, RH = { 0.670231729055258, 0.00740740740740729 }, Flags = { Linear = true } },
									[1] = { 1, LH = { 0.75222816399287, 0.444444444444445 } }
								}
							},
							SplineColor = { Red = 204, Green = 204, Blue = 204 },
							NameSet = true,
						},
						ColorCurves1Red = LUTBezier {
							KeyColorSplines = {
								[0] = {
									[0] = { 0, RH = { 0.670231729055258, 0.00740740740740729 }, Flags = { Linear = true } },
									[1] = { 1, LH = { 0.75222816399287, 0.444444444444445 } }
								}
							},
							SplineColor = { Red = 255, Green = 52, Blue = 52 },
							NameSet = true,
						}
					},
				},
				Clamp = BrightnessContrast {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						EffectMask = Input {
							SourceOp = "CL",
							Source = "Output",
						},
						MultiplyByMask = Input { Value = 1, },
						MaskChannel = Input { Value = 5, },
						ClipBlack = Input { Value = 1, },
						Input = Input {
							SourceOp = "CS",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -122.162, 309.942 } },
				},
				FN = FastNoise {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						MaskChannel = Input { Value = 5, },
						Width = Input {
							Value = 240,
							Expression = "GLOW.Background.OriginalWidth/8",
						},
						Height = Input { Expression = "Width", },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Detail = Input { Value = 10, },
						Contrast = Input { Value = 2.77, },
						LockXY = Input { Value = 0, },
						XScale = Input { Value = 8.54, },
						YScale = Input { Value = 0, },
						Seethe = Input { Value = 0.25, },
						SeetheRate = Input { Value = 0.15, },
						Color1Red = Input { Value = 1, },
						Color1Green = Input { Value = 1, },
						Color1Blue = Input { Value = 1, },
						Color2Red = Input { Value = 0, },
						Color2Green = Input { Value = 0, },
						Color2Blue = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { -395.168, 309.942 } },
				},
				CS = CoordSpace {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Shape = Input { Value = 1, },
						Input = Input {
							SourceOp = "FN",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -264.486, 309.942 } },
				},
				SetDomain2 = SetDomain {
					CtrlWShown = false,
					Inputs = {
						Mode = Input { Value = FuID { "Set" }, },
						Input = Input {
							SourceOp = "Merge2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 418, 368.821 } },
				},
				Merge2 = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.4, },
						Background = Input {
							SourceOp = "Down",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "Transform3",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.3, 0.7 },
							Expression = "GLOW:GetSourceTool(\"Controls\").PointIn1",
						},
						Size = Input {
							Value = 2.5,
							Expression = "SizeCopy",
						},
						Gain = Input { Value = 0, },
						FilterMethod = Input { Value = 6, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 418, 309.942 } },
					UserControls = ordered() {
						SizeCopy = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "SliderControl",
							INP_MaxScale = 5,
							INP_Default = 2.5,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							ICD_Center = 1,
							LINKS_Name = "SizeCopy",
						}
					}
				},
				ColorGain3 = ColorGain {
					NameSet = true,
					Inputs = {
						LiftNest = Input { Value = 0, },
						SaturationRed = Input { Value = 5, },
						SaturationGreen = Input { Value = 5, },
						SaturationBlue = Input { Value = 5, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.125, 0.75 },
											{ 0.375, 0.25 },
											{ 0.5, 0 }
										}
									},
									{
										Points = {
											{ 0.5, 0 },
											{ 0.625, 0.25 },
											{ 0.875, 0.75 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						Input = Input {
							SourceOp = "Clamp",
							Source = "Output",
						},
						GainBlue = Input { Value = 0.85, },
					},
					ViewInfo = OperatorInfo { Pos = { 17.025, 309.942 } },
					UserControls = ordered() {
						GainRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Red",
						},
						GainGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Green",
						},
						GainBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Blue",
						},
						GainAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Alpha",
						},
						GammaRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Red",
						},
						GammaGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Green",
						},
						GammaBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Blue",
						},
						GammaAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Alpha",
						},
						PreDividePostMultiply = {
							INP_MaxAllowed = 1,
							INP_Integer = false,
							INPID_InputControl = "CheckboxControl",
							INP_MaxScale = 1,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							CBC_TriState = false,
							LINKS_Name = "Pre-Divide / Post-Multiply",
						}
					}
				},
				BG = Background {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Width = Input {
							Value = 240,
							Expression = "GLOW.Background.OriginalWidth/8",
						},
						Height = Input { Expression = "Width", },
						["Gamut.SLogVersion"] = Input { Value = FuID { "SLog2" }, },
						Type = Input { Value = FuID { "Gradient" }, },
						GradientType = Input { Value = FuID { "Radial" }, },
						Start = Input { Value = { 0.5, 0.5 }, },
						Gradient = Input {
							Value = Gradient {
								Colors = {
									[0] = { 1, 1, 1, 1 },
									[1] = { 0, 0, 0, 1 }
								}
							},
						},
						GradientInterpolationMethod = Input { Value = FuID { "LAB" }, },
					},
					ViewInfo = OperatorInfo { Pos = { -419.765, 137.464 } },
				},
				ColorGain1 = ColorGain {
					CtrlWShown = false,
					Inputs = {
						LiftNest = Input { Value = 0, },
						SaturationRed = Input { Value = 5, },
						SaturationGreen = Input { Value = 5, },
						SaturationBlue = Input { Value = 5, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.125, 0.75 },
											{ 0.375, 0.25 },
											{ 0.5, 0 }
										}
									},
									{
										Points = {
											{ 0.5, 0 },
											{ 0.625, 0.25 },
											{ 0.875, 0.75 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						Input = Input {
							SourceOp = "Transform1",
							Source = "Output",
						},
						GainBlue = Input { Value = 0.9, },
						GammaRed = Input { Value = 0.9, },
						GammaGreen = Input { Value = 0.9, },
					},
					ViewInfo = OperatorInfo { Pos = { 36.4012, 55.653 } },
					UserControls = ordered() {
						GainRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Red",
						},
						GainGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Green",
						},
						GainBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Blue",
						},
						GainAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Alpha",
						},
						GammaRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Red",
						},
						GammaGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Green",
						},
						GammaBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Blue",
						},
						GammaAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Alpha",
						},
						PreDividePostMultiply = {
							INP_MaxAllowed = 1,
							INP_Integer = false,
							INPID_InputControl = "CheckboxControl",
							INP_MaxScale = 1,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							CBC_TriState = false,
							LINKS_Name = "Pre-Divide / Post-Multiply",
						}
					}
				},
				Transform2 = Transform {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Size = Input { Value = 5, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "BG",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 52.0603, 137.464 } },
				},
				CL = CineonLog {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						RedBlackLevel = Input { Value = 0, },
						RedWhiteLevel = Input { Value = 1023, },
						RedSoftClipKnee = Input { Value = 100, },
						RedFilmStockGamma = Input { Value = 0.962, },
						RedConversionGamma = Input { Value = 2.596, },
						SLogVersion = Input { Value = FuID { "SLog2" }, },
						Input = Input {
							SourceOp = "BG",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -122.162, 243.437 } },
				},
				Transform1 = Transform {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Size = Input { Value = 5, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "ColorCurves1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -98.5767, 55.653 } },
				},
				Blur1 = Blur {
					CtrlWShown = false,
					Inputs = {
						Filter = Input {
							Value = FuID { "Fast Gaussian" },
							Expression = "tonumber(bmd._VERSION:sub(1,2)) >= 15 and \"Fast Gaussian\" or \"Gaussian\"",
						},
						XBlurSize = Input {
							Value = 10,
							Expression = "2400/BG.Width",
						},
						ClippingMode = Input { Value = FuID { "None" }, },
						Input = Input {
							SourceOp = "BrightnessContrast2",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 285.651, 55.653 } },
				},
				BrightnessContrast2 = BrightnessContrast {
					CtrlWShown = false,
					Inputs = {
						Gamma = Input { Value = 2, },
						Input = Input {
							SourceOp = "ColorGain1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 164.086, 55.653 } },
				},
				ColorGain2 = ColorGain {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						LiftNest = Input { Value = 0, },
						SaturationRed = Input { Value = 5, },
						SaturationGreen = Input { Value = 5, },
						SaturationBlue = Input { Value = 5, },
						ColorRanges = Input {
							Value = ColorCurves {
								Curves = {
									{
										Points = {
											{ 0, 1 },
											{ 0.125, 0.75 },
											{ 0.375, 0.25 },
											{ 0.5, 0 }
										}
									},
									{
										Points = {
											{ 0.5, 0 },
											{ 0.625, 0.25 },
											{ 0.875, 0.75 },
											{ 1, 1 }
										}
									}
								}
							},
						},
						Input = Input {
							SourceOp = "Transform2",
							Source = "Output",
						},
						GainRed = Input { Value = 0.9, },
						GainGreen = Input { Value = 0.8, },
					},
					ViewInfo = OperatorInfo { Pos = { 223.87, 137.464 } },
					UserControls = ordered() {
						GainRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Red",
						},
						GainGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Green",
						},
						GainBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Blue",
						},
						GainAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = -1000000,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 1,
							LINKS_Name = "Gain Alpha",
						},
						GammaRed = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 0,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Red",
						},
						GammaGreen = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 1,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Green",
						},
						GammaBlue = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 2,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Blue",
						},
						GammaAlpha = {
							INP_MaxAllowed = 1000000,
							INP_Integer = false,
							INPID_InputControl = "ColorControl",
							CLRC_ColorSpace = "HSV",
							INP_MaxScale = 1,
							INP_Default = 1,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							IC_ControlID = 3,
							CLRC_ShowWheel = true,
							IC_ControlGroup = 2,
							LINKS_Name = "Gamma Alpha",
						},
						PreDividePostMultiply = {
							INP_MaxAllowed = 1,
							INP_Integer = false,
							INPID_InputControl = "CheckboxControl",
							INP_MaxScale = 1,
							INP_Default = 0,
							INP_MinScale = 0,
							INP_MinAllowed = 0,
							LINKID_DataType = "Number",
							CBC_TriState = false,
							LINKS_Name = "Pre-Divide / Post-Multiply",
						}
					}
				},
				DirectionalBlur1 = DirectionalBlur {
					Inputs = {
						Type = Input { Value = 1, },
						Input = Input {
							SourceOp = "ColorGain3",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 158.508, 309.942 } },
				},
				Down = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Blend = Input { Value = 0.12, },
						Background = Input {
							SourceOp = "GLOW",
							Source = "Output",
						},
						Foreground = Input {
							SourceOp = "ColorGain2",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.7, 0.3 },
							Expression = "GLOW:GetSourceTool(\"Controls\").PointIn4",
						},
						Size = Input {
							Value = 4,
							Expression = "SizeCopy",
						},
						Gain = Input { Value = 0, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 418, 137.464 } },
					UserControls = ordered() {
						SizeCopy = {
							INP_Default = 4,
							INP_Integer = false,
							ICD_Center = 1,
							INPID_InputControl = "SliderControl",
							INP_MinScale = 0,
							LINKID_DataType = "Number",
							INP_MaxScale = 5,
							LINKS_Name = "SizeCopy",
						}
					}
				},
				GLOW = Merge {
					CtrlWShown = false,
					NameSet = true,
					Inputs = {
						Foreground = Input {
							SourceOp = "Blur1",
							Source = "Output",
						},
						Center = Input {
							Value = { 0.3, 0.7 },
							Expression = "self:GetSourceTool(\"Controls\").PointIn1",
						},
						Size = Input { Value = 5, },
						Gain = Input { Value = 0, },
						FlattenTransform = Input { Value = 1, },
						PerformDepthMerge = Input { Value = 0, },
					},
					ViewInfo = OperatorInfo { Pos = { 418, 55.653 } },
					UserControls = ordered() {
						Controls = {
							LINKID_DataType = "Image",
							LINKS_Name = "Controls",
							IC_ControlPage = 0,
							INPID_InputControl = "ImageControl",
							INP_Default = 0,
						}
					}
				},
				CineonLog1 = CineonLog {
					CtrlWShown = false,
					Inputs = {
						RedBlackLevel = Input { Value = 0, },
						RedWhiteLevel = Input { Value = 1023, },
						RedFilmStockGamma = Input { Value = 2.59, },
						RedConversionGamma = Input { Value = 0.33, },
						SLogVersion = Input { Value = FuID { "SLog2" }, },
						Input = Input {
							SourceOp = "BG",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { -241.656, 8.69412 } },
				},
				Transform3 = Transform {
					Inputs = {
						FilterMethod = Input { Value = 1, },
						FlattenTransform = Input { Value = 1, },
						Input = Input {
							SourceOp = "DirectionalBlur1",
							Source = "Output",
						},
					},
					ViewInfo = OperatorInfo { Pos = { 297.333, 309.942 } },
				}
			},
		}
	},
	ActiveTool = "ml_LFGlow"
}