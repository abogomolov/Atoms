Install FFmpeg on Ubuntu and Linux Mint

I will install FFmpeg from the default repositories. Open a new terminal (CTRL+ALT+T) and then run the following commands.

    $ sudo apt update
    $ sudo apt install ffmpeg
    $ ffmpeg -version


Install FFmpeg on Debian

The FFmpeg package is included in the official Debian repositories and can be installed using the package manager as shown.

    $ sudo apt update
    $ sudo apt install ffmpeg
    $ ffmpeg -version


Install FFmpeg on CentOS and RHEL

To install FFmpeg on CentOS and RHEL distributions, you need to enable EPEL and RPM Fusion repository on the system using the following commands.

To install and enable EPEL, use the following command.

    # yum install epel-release

After enabling repositories, run the following command to install FFmpeg:

    # yum install ffmpeg ffmpeg-devel
    # ffmpeg -version


Install FFmpeg on Fedora

On Fedora, you need to install and enable RPM Fusion to install FFmpeg as shown.

    $ sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    $ sudo dnf install ffmpeg ffmpeg-devel
    $ ffmpeg -version


Install FFmpeg on Arch Linux

    $ sudo pacman -S ffmpeg
    $ yay -S ffmpeg-git
    $ yay -S ffmpeg-full-git
    $ ffmpeg -version


Install FFmpeg on openSUSE

-------------- On openSUSE Tumbleweed --------------

    $ sudo zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/' packman
    $ sudo zypper refresh
    $ sudo zypper install --from packman ffmpeg
    $ ffmpeg -version

-------------- On openSUSE Leap --------------

    $ sudo zypper addrepo -cfp 90 'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_$releasever/' packman
    $ sudo zypper refresh
    $ sudo zypper install --from packman ffmpeg
    $ ffmpeg -version



FFmpeg Compiling from Source

Compiling software from a source is not the easiest thing in the world, but with the right instructions, we will be able to do it. First, make sure your system meets all the dependencies. The installation of these dependencies can be done with the help of the following commands.

First, tell the system to pull down the latest packages.

    $ sudo apt-get update

Install the dependencies with the following command.

-------------- On Debian & Ubuntu --------------

    $ sudo apt-get -y install autoconf automake build-essential libass-dev libfreetype6-dev libgpac-dev \
    libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libx11-dev \
    libxext-dev libxfixes-dev pkg-config texi2html zlib1g-dev

-------------- On CentOS and RHEL --------------

    # yum install glibc gcc gcc-c++ autoconf automake libtool git make nasm pkgconfig SDL-devel \
    a52dec a52dec-devel alsa-lib-devel faac faac-devel faad2 faad2-devel freetype-devel giflib gsm gsm-devel \
    imlib2 imlib2-devel lame lame-devel libICE-devel libSM-devel libX11-devel libXau-devel libXdmcp-devel \
    libXext-devel libXrandr-devel libXrender-devel libXt-devel libogg libvorbis vorbis-tools mesa-libGL-devel \
    mesa-libGLU-devel xorg-x11-proto-devel zlib-devel libtheora theora-tools ncurses-devel libdc1394 libdc1394-devel \
    amrnb-devel amrwb-devel opencore-amr-devel

Then use the following command to create a new directory for the FFmpeg sources. This is the directory where the source files will be downloaded.

    $ mkdir ~/ffmpeg_sources

Now compile and install yasm assembler used by FFmpeg by running the following commands.

    $ cd ~/ffmpeg_sources
    $ wget http://www.tortall.net/projects/yasm/releases/yasm-1.3.0.tar.gz
    $ tar xzvf yasm-1.3.0.tar.gz
    $ cd yasm-1.3.0
    $ ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin"
    $ make
    $ make install
    $ make distclean
    $ export "PATH=$PATH:$HOME/bin"

After you have successfully installed the yasm assembler it is time to install some various encoders that will be used with the specific FFmpeg tools. Use the following commands to install the H.264 video encoder.

    $ cd ~/ffmpeg_sources
    $ wget http://download.videolan.org/pub/x264/snapshots/last_x264.tar.bz2
    $ tar xjvf last_x264.tar.bz2
    $ cd x264-snapshot*
    $ ./configure --prefix="$HOME/ffmpeg_build" --bindir="$HOME/bin" --enable-static
    $ make
    $ make install
    $ make distclean

Another nice useful encoder is the libfdk-aac AAC audio encoder.

    $ cd ~/ffmpeg_sources
    $ wget -O fdk-aac.zip https://github.com/mstorsjo/fdk-aac/zipball/master
    $ unzip fdk-aac.zip
    $ cd mstorsjo-fdk-aac*
    $ autoreconf -fiv
    $./configure --prefix="$HOME/ffmpeg_build" --disable-shared
    $ make
    $ make install
    $ make distclean

Install libopus audio decoder and encoder.

    $ cd ~/ffmpeg_sources
    $ wget http://downloads.xiph.org/releases/opus/opus-1.1.tar.gz
    $ tar xzvf opus-1.1.tar.gz
    $ cd opus-1.1
    $ ./configure --prefix="$HOME/ffmpeg_build" --disable-shared
    $ make
    $ make install
    $ make distclean

Now, it’s time to install ffmpeg from the source.

    $ cd ~/ffmpeg_sources
    $ wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2
    $ tar xjvf ffmpeg-snapshot.tar.bz2
    $ cd ffmpeg
    $ PKG_CONFIG_PATH="$HOME/ffmpeg_build/lib/pkgconfig"
    $ export PKG_CONFIG_PATH
    $ ./configure --prefix="$HOME/ffmpeg_build" --extra-cflags="-I$HOME/ffmpeg_build/include" \
    --extra-ldflags="-L$HOME/ffmpeg_build/lib" --bindir="$HOME/bin" --extra-libs="-ldl" --enable-gpl \
    --enable-libass --enable-libfdk-aac --enable-libfreetype --enable-libmp3lame --enable-libopus \
    --enable-libtheora --enable-libvorbis --enable-libvpx --enable-libx264 --enable-nonfree --enable-x11grab
    $ make
    $ make install
    $ make distclean
    $ hash -r

Note: If you have not installed certain encoders, make sure to remove ‘–enable-encoder_name‘ from the above ‘./configure‘ command so the installation is done without any problem.

There are many encoders that you can install, but fur the purpose of this article I am not going to install all of them, but you can install them using the following official guides.

https://www.tecmint.com/install-ffmpeg-in-linux/