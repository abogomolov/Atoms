-- "Show Docs Online" menu item

platform = (FuPLATFORM_WINDOWS and "Windows") or (FuPLATFORM_MAC and "Mac") or (FuPLATFORM_LINUX and "Linux")
function OpenURL(siteName, path)
	if platform == "Windows" then
		-- Running on Windows
		command = "explorer \"" .. path .. "\""
	elseif platform == "Mac" then
		-- Running on Mac
		command = "open \"" .. path .. "\" &"
	elseif platform == "Linux" then
		-- Running on Linux
		command = "xdg-open \"" .. path .. "\" &"
	else
		print("[Error] There is an invalid Fusion platform detected")
		return
	end
	os.execute(command)
	-- print("[Launch Command] ", command)
	print("\n[Opening URL] [" .. siteName .. "] ", path)
end

OpenURL("Vonk Online Docs", "https://docs.google.com/document/d/1U9WfdHlE1AZHdU6_ZQCB1I2nSa5I7TyHG2vKMi2I7v8/edit")
