-- "Show Macros Folder" menu item

local path = app:MapPath("Macros:/Kartaverse/Vonk Ultra/")
print("\n[Show Macros Folder] ", path)

if bmd.direxists(path) == false then
    bmd.createdir(path)
    print("[Created Macros Folder] ", path)
end

bmd.openfileexternal("Open", path)
