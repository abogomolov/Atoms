Atom {
	Name = "Change Strings",
	Category = "Scripts/Comp",
	Author = "Noah Hähnel",
	Version = 2.06,
	Date = {2022, 12, 2},
	Description = [[<strong>Change Strings</strong> is a Search and Replace Tool designed for Fusion. This can help you to quickly change selected Loader filepaths, increase versions, and stay organized. <a href="https://noahhaehnel.com/blog/manual-changestrings/">You can see it in action and read all about it here.</a>
 <br> <br>
This script can be used to <strong>batch replace</strong> strings (text) in:

<ul>
	<li>Loader, Saver, and FBX/Alembic Filepaths</li>
	<li>Node Names</li>
	<li>Expressions</li>
	<li>Text+/Text3D</li>
</ul>
It also contains many more features. Including:

<ul>
	<li>Increase or Decrease Versions</li>
	<li>Switch Text Patterns around (enable Space Splitter)</li>
	<li>QuickSelect Buttons for Loaders, Savers, FBX/ABC tools</li>
	<li>QuickSelect by Search</li>
	<li>A Textbox to use Change Strings for Text from outside of Fusion</li>
	<li>A proven UI with many Quality of Life enhancements</li>
</ul>

Any isues, feature requests or other messages can be reported to me by contacting me through any of the channels listed on my 
<a href="https://noahhaehnel.com/">website.</a> I hope this script saves you as much time as it has saved me.]],
	Donation = {
		URL = [[https://noahhaehnel.gumroad.com/]],
		Amount = "5",
	},

	Deploy = {
		"Scripts/Comp/ChangeStrings.lua",
	},
}
