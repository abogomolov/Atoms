# kvrKeyboardMaestro fuses

v5.0 2022-05-04 04.04 PM  
By Andrew Hazelden  

## Overview:

A pair of fuses that let you run a macOS based Keyboard Maestro macro from inside Fusion's node graph. This unlocks node-based GUI automation techniques such as controlling external applications via simulating keyboard and mouse actions.

## Installation:

1. This fuse requires the installation of "Keyboard Maestro" for macOS:
[https://www.keyboardmaestro.com/main/](https://www.keyboardmaestro.com/main/)

## Usage:

1. Create a new comp.

2. Add a "kvrKeyboardMaestro" image or text based fuse to the comp.

3. In the Inspector window, enter the name of the Keyboard Maestro macro you want launched into the text field labelled "Macro Name".

4. The first time the macro is run, you need to approve a macOS security message that says:

        "Fusion.app" wants access to control "Keyboard Maestro.app". Allowing control will provide access to documents and data in "Keyboard Maestro.app" and to perform actions within that app.

    You need to click the "OK" button to continue.

5. In Keyboard Maestro's Editor program create a corresponding macro that will perform the actual GUI automation tasks. Click the "New Action" button in the Editor window to add each step you'd like the macro to carry out. You could also use the "Record" button to save a series of mouse interactions or keyboard button presses.

6. When your macro is complete, switch out of the "Edit" state and then try out the macro.

## Tips:

If you only want Keyboard Maestro launched when a Fusion batch render is occurring, uncheck the "Interactive Render" checkbox.

The kvrKeyboardMaestro nodes have a Text data type based input connection named "Macro". This input connection supports the use of external Vonk nodes like "TextCreate" as the source of the macro name that will be launched when this node is rendered.
