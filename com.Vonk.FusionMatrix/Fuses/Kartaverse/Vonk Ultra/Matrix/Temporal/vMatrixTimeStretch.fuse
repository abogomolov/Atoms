-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMatrixTimeStretch"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Matrix\\Temporal",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Time based operation on Matrix.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Input", "Input", {
        LINKID_DataType = "Text",
        INP_SendRequest = false,
        INP_Required = false,
        LINK_Main = 1
    })

    InTime = self:AddInput("Time", "Time", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_MinScale = -100.0,
        INP_MaxScale = 100.0,
        INP_Default = 0.0,
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end
        InText:SetAttrs({LINK_Visible = visible})
        InTime:SetAttrs({LINK_Visible = visible})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local t = InTime:GetValue(req).Value
    local out = InText:GetSource(t, req:GetFlags())
    OutText:Set(req, out)
end
