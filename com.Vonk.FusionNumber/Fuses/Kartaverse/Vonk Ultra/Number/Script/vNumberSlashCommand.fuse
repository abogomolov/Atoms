--[=[--
SlashCommand Examples:

/for (selected|visible|all) [tooltype[,tooltype...]] [where <condition>] <command> [ & <command>...]
Supported commands:
	animate <input> [(with <modifier>|remove)] [force]
	color [tile <color>] [text <color>] [fill <color>]
	select [(add|remove)]
	set <input> ([at <time>] to <value>|expression <exp>)

/footage list

/snapshot

/ping <hostname>

/shell <command> <options>

/atom Reactor:/Atoms/Reactor/<SomeAtomPackageName>.atom

--]=]--

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberSlashCommand"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Script",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Run a Console Fuse SlashCommand as a node.",
    REGS_OpIconString = FUSE_NAME,

    -- Should the current time setting be cached?
    -- REG_TimeVariant = true,
    -- REG_Unpredictable = true,

    -- Icon shown in the "Select Tool" dialog and the "Tile Picture"
    REGS_IconID = "Icons.Tools.Icons.RunCommand"
})

function Create()
    -- [[ Creates the user interface. ]]
    InNumber = self:AddInput("Number", "Number", {
        LINKID_DataType = "Number",
        --INP_Required = false,
        LINK_Main = 1,
    })

    InText = self:AddInput("Text" , "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 5,
        LINK_Main = 2
    })

    InDisplayLines = self:AddInput("Display Lines", "DisplayLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "ScrewControl",
        INP_Integer = true,
        INP_MaxScale = 100,
        INP_MinAllowed = 1,
        INP_Default = 25,
        LINK_Visible = true,
        INP_Passive = true,
        INP_DoNotifyChanged  = true,
    })

    InWrapLines = self:AddInput("Wrap Lines", "WrapLines", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    self:AddInput([=[SlashCommand Examples:
/footage list

/for (selected|visible|all) [tooltype[,tooltype...]] [where <condition>] <command> [ & <command>...]
Supported commands:
    animate <input> [(with <modifier>|remove)] [force]
    color [tile <color>] [text <color>] [fill <color>]
    select [(add|remove)]
    set <input> ([at <time>] to <value>|expression <exp>)]=], "help1", {
        LINKID_DataType = "Text",
        INPID_InputControl = "LabelControl",
        LBLC_MultiLine = true,
        INP_External = false,
        INP_Passive = true,
        IC_NoLabel = true,
        IC_NoReset = true,
    })

    OutputNumber = self:AddOutput("OutputNumber", "OutputNumber" , {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })

    OutText = self:AddOutput("OutputText" , "OutputText" , {
        LINKID_DataType = "Text",
        LINK_Main = 2
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then visible = true else visible = false end

        InText:SetAttrs({LINK_Visible = visible})
    elseif inp == InWrapLines then
        local wrap
        if param.Value == 1.0 then wrap = true else wrap = false end
        InText:SetAttrs({TEC_Wrap = wrap})

        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    elseif inp == InDisplayLines then
        -- Update the TEC Lines value dynamically
        -- Inspired by vNumberXSheet.fuse
        local lines = InDisplayLines:GetSource(time, REQF_SecondaryTime).Value
        InText:SetAttrs({TEC_Lines = lines})
    
        -- Toggle the visibility to refresh the inspector view
        InText:SetAttrs({IC_Visible = false})
        InText:SetAttrs({IC_Visible = true})
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local num = InNumber:GetValue(req).Value

    local txt_str = InText:GetValue(req).Value
    local out_str = "0"

    local script = _Lua [=[
local P, S, C, Ct = lpeg.P, lpeg.S, lpeg.C, lpeg.Ct

local function token(patt) return C(patt) end
local doubleq = P '"' * ((1 - S '"\r\n\f\\') + (P '\\' * 1)) ^ 0 * '"'
local white = S('\r\n\f\t ')^1
local word = token((1 - S("' \r\n\f\t\""))^1)
local str = token(doubleq)

local arg_parser = Ct((str + white + word) ^ 0)

local args = {}

args[0] = text

for i,arg in ipairs(arg_parser:match(text)) do
    args[i] = arg
end

local luaname = comp:MapPath("Scripts:SlashCommand".. args[1] .. ".lua")
local pyname = comp:MapPath("Scripts:SlashCommand".. args[1] .. ".py")

if bmd.fileexists(luaname) then
    comp:RunScript(luaname, { args = args })
elseif bmd.fileexists(pyname) then
    comp:RunScript(pyname, { args = args })
else
    print("Unknown command: " .. args[1])
end
    ]=]

    if txt_str and (txt_str:sub(1,1) == "/") then
        print("> " .. tostring(txt_str) .. "\n")

        local run_str = "local text = [=[" .. tostring(txt_str) .. "]=]\n" .. tostring(script)
        -- dump(run_str)

        self.Comp:Execute(run_str)
        out_str = "1"
    end

    OutText:Set(req, Text(out_str))

    local out_num = Number(num)
    OutputNumber:Set(req, out_num)
end
