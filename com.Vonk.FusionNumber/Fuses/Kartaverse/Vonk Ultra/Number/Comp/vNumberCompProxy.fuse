-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vNumberCompProxy"
DATATYPE = "Number"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Number\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Returns the comp's Proxy state.",
    REGS_OpIconString = FUSE_NAME,
    REG_TimeVariant = true, -- required to disable caching of the current time parameter
    REGB_Temporal = true, -- ensures reliability in Resolve 15
})

function Create()
    -- [[ Creates the user interface. ]]
    InTrue = self:AddInput("True", "True", {
        IC_Steps = 201,
        INPID_InputControl = "SliderControl",
        INP_Default = 1,
        INP_MaxScale = 100,
        INP_MinScale = -100,
        LINKID_DataType = "Number",
        LINK_Main = 1,
    })

    InFalse = self:AddInput("False", "False", {
        IC_Steps = 201,
        INPID_InputControl = "SliderControl",
        INP_Default = 0,
        INP_MaxScale = 100,
        INP_MinScale = -100,
        LINKID_DataType = "Number",
        LINK_Main = 2,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    Output = self:AddOutput("Output" , "Output" , {
        LINKID_DataType = "Number",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]

    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then
            visible = true
        else
            visible = false
        end

        InTrue:SetAttrs({LINK_Visible = visible})
        InFalse:SetAttrs({LINK_Visible = visible})
    end
end
function Process(req)
    -- [[ Creates the output. ]]
    local prx = self.Comp.Proxy

    local result
    if prx == true then
        -- 1, or any other postive number
        result = tonumber(InTrue:GetValue(req).Value)
    else
        -- 0 or nil
        result = tonumber(InFalse:GetValue(req).Value)
    end

    local out = Number(result)
    Output:Set(req, out)
end
