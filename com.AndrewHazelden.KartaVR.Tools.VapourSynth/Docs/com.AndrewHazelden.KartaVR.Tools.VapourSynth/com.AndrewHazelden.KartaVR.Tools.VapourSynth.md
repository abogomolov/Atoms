# VapourSynth for Fusion
v1.3 2022-10-31 06.10 PM  
By Andrew Hazelden [andrew@andrewhazelden.com](mailto:andrew@andrewhazelden.com)

## Overview

The VapourSynth fuse lets you interactively render .vpy scripts from inside a Fusion Studio node graph using an approach that is similar in concept to a UNIX image pipe. 

## Requirements

Fusion Studio v16-18+ is required to use the fuse due to the use of the Fuse API PutFrame() function. This tool does not work in Resolve due to limitations in the Resolve API that stops the PutFrame() function from saving out a .y4m format.

Note: In Fusion Studio, this fuse reads in the first frame of the VSPIPE exported .y4m video file using the fuse API's GetFrame() function. In Resolve's version of the Fuse API, movie formats aren't supported so (in theory) you'd need to bounce the .y4m file through ffmpeg to convert it to an image format before reading it.

Note: The .y4m video format doesn't support alpha channels. If you modify the fuse's source code to export an image type that supports alpha channels, then you'd have to strip that alpha channel data out (inside the fuse) before you feed the image to vspipe via the CLI.

## VapourSynth Fuse Screenshot

![VapourSynth for Fusion](VapourSynth-For-Fusion.png)

## Exposed VPY Variables

### Read the temporary filename written to disk by Fusion

	Variable:
	in_filename

	Result:
	/private/var/folders/rv/sj_pltzx0vb6slldpwfqpk780000gn/T/VapourSynth/in_ad54eca3-0b6c-4f8c-a20f-443719589a22.0.jpg



### Read the Fusion node name

	Variable:
	node


	Result:
	VapourSynth1


## VPY Script Code Samples:


### Display Version Info VPY Script:

	# -----------------------------
	# Display Version Info VPY Script
	# -----------------------------
	from vapoursynth import core

	# Load footage from Fusion
	clip = core.ffms2.Source(source=in_filename)

	# Overlay the VapourSynth Version
	clip = clip.text.Text(core.version())

	# Send the results back
	clip.set_output()

	# VapourSynth Video Processing Library
	# Copyright (c) 2012-2021 Fredrik Mellbin
	# Core R53
	# API R3.6
	# Options: -
	# -----------------------------

### Display Clip Info VPY Script:

	# -----------------------------
	# Display Clip Info VPY Script:
	# -----------------------------
	from vapoursynth import core

	# Load footage from Fusion
	clip = core.ffms2.Source(source=in_filename)

	# Overlay stats on the image
	clip = clip.text.ClipInfo()

	# Send the results back
	clip.set_output()
	# -----------------------------


### Display Node Name VPY Script:

	# -----------------------------
	# Display Node Name VPY Script
	# -----------------------------
	from vapoursynth import core

	# Load footage from Fusion
	clip = core.ffms2.Source(source=in_filename)

	# Overlay the node name on the image
	clip = clip.text.Text(node)

	# Send the results back
	clip.set_output()


