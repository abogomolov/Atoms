{[[Using the viewpoint or copying the PoV is not limited to cameras only. You can use lights, Transform3D and Merge3D too by using the 'other' sub menu. This makes it super super easy to set the PoV for a camera, adjust light direction or moving an entire 3D scene to match with another 3D scene.

-- Rjun Rajput]]}
