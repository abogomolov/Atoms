{[[If you want to control how smooth/sharp your polygon/bspline mask points need to be, select your point, press and hold W, now drag your mouse to adjust smoothness/sharpness.

-- Rjun Rajput]]}
