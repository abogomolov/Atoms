{[[To query a node's image size in an expression, use [nodename].Output.OriginalHeight and [nodename].Output.OriginalWidth
You can use just Height and Width, but the dimensions will change in proxy mode if you do.

-- Bryan Ray]]}
