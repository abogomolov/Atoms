{[[Ever wonder why fusion imports your ABC/OBJ/FBX as a triangulated mesh, even though it was exported with quads? Well, Fun Fact: If a single triangle exists in the model, fusion will triangulate everything on import. If the model is 100% quads, fusion will import quads.

-- Dan De'Entremont]]}
