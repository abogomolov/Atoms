DEBUG = true
SCRIPTNAME = "Make Local"
VERSION = "2.45"
DATE = "2022-11-14"

--[[--
	MakeLocal script for Fusion by Bryan Ray — bray@musevfx.com
	with contributions by Tom "TheBloke" Jobbins and Steve Oakley
	Tested in Windows and MacOS, but it should, theoretically, work in Linux also.
	
	This tool script determines if the contents of a Loader are local to the 
	shot, and if not, moves the asset into the local folder structure. It 
	requires a TD to make some adjustments to match their pipeline. See below.
	
	The script is intended to be run from a button on the Loader. A Default
	for Loaders that includes the button is distributed with the script.
	Put the Loader_Loader.setting file in the Defaults folder and this script
	in Scripts/Support. 
	
	You may have to create the Support folder, as it is
	not a part of a typical Fusion or Resolve install.
	
SCRIPT OPERATION:
	Upon clicking the Make Local button on the Loader, the script compares the Clip's
	current location to the .comp file's. If it is determined that the Clip is not 
	in the same folder tree as the comp (as defined by the pipeline global variables 
	described below), a pop-up UI is presented to the user.
	
	In this UI, the user may enter a subfolder name in which the asset should be stored and
	indicate whether or not it is an image sequence. The script attempts to determine the
	presence of a sequence on its own and will pre-check the box. 
	
	Upon clicking OK, the necessary folders are created, if they don't already exist, the 
	asset is copied, and the Loader updated to reflect the new location.
	
	Changelog:
		v2.45: Converted Support folder name to title case for case-sensitive filesystem usage.
		v2.43-2.44: Refined element sub-folder naming. Now trims off extension and frame number.
		v2.1 - 2.42: Bug fixes on MacOS and refactoring
		v2.0: Generalized for non Muse VFX pipelines
		v2.1: Added Mac and Linux compatibility, added documentation
		v2.2: Bug fixes and refactoring, debug mode
	

======== TD DOCUMENTATION ==============

Determining whether or not an asset is local to the shot involves comparing 
its location to that of the .comp file.

If the asset and comp are located in the same subtree of the directory 
structure, then the asset is considered to be local to the shot and will
not be moved. 

The following pipeline global variables are used to customize the script to your
pipeline. The assignments happen immediately after this comment block.


COMPDEPTH: How many folders deep is the comp in the shot structure? 
For instance, if your folder structure looks like this:
	G:\project\Shot\fusionComps\shot_mycomp_v01.comp
then the comp depth is 2. Setting the COMPDEPTH = 2 and keeping the default value of 
ASSETFOLDER = 'elements' will place the asset in this location:
	G:\project\Shot\elements\asset

The default value of 0 will place assets in a folder next
to the comp file:
	G:\project\Shot\fusionComps\elements\asset
	

ASSETFOLDER: This is a location relative to the root shot folder where 
assets are to be stored. If this folder does not exist, it will be created. To place
assets in the base folder instead of creating a subfolder, set it to an empty string: 
	ASSETFOLDER = ''

With COMPDEPTH = 0 and ASSETFOLDER = '', the asset will be copied into the same folder
as the .comp file.

STILLSFOLDER: This is the folder name that will be pre-loaded into the UI
if the script detects a single-frame image file. By default this is an empty string, but if you wanted
to tidy up your elements folder by putting single-frame elements into a sub-folder called 'stills', you
could set STILLSFOLDER = 'stills'


VIDEOFOLDER: This is the folder name that will be pre-loaded into the UI 
if the script detects a video file.By default this is an empty string, but if you wanted
to tidy up your elements folder by putting video elements into a sub-folder called 'videos', you
could set VIDEOFOLDER = 'videos'


VIDEO: A table of recognized video and audio extensions. You can add file extensions here
in order to cause more formats to be recognized as video files.


FRAMEDELIMITERS: A table of possible characters used to set off the frame number from the 
rest of the filename. This is used to simplify the element sub-folder name. This prevents
the script from creating folders like "myImageSequence_\" or "myImageSequence.\" You
can add additional characters to the table if your pipeline uses an unusual delimiter
character.
  --]]--

--========= PIPELINE GLOBALS ==============
  
COMPDEPTH = 0
ASSETFOLDER = 'elements'
STILLSFOLDER = ''
VIDEOFOLDER = ''

VIDEO = {"mov", "avi", "mxf", "mkv", "vdr", "wav", "mp4", "qt", "3gp", "mpg", "mpeg", "m4v", }

FRAMEDELIMITERS = {"_", "."}
  
--=========================================
-- You shouldn't need to change anything below this line


SEPARATOR = package.config:sub(1,1)	-- Folder separator used by the Operating System. 

-- Find out the current operating system platform. 
local PLATFORM = (FuPLATFORM_WINDOWS and 'Windows') or (FuPLATFORM_MAC and 'Mac') or (FuPLATFORM_LINUX and 'Linux')


function main()
	dprint("\n"..SCRIPTNAME.." v"..VERSION..", "..DATE)
	
	local tu = fusion.TIME_UNDEFINED

	if not tool then tool = comp.ActiveTool end
	dprint("\ntool:")
	ddump(tool)

	-- Get clip path
	local attrs = tool:GetAttrs()
	local path = comp:MapPath(tool.Clip[tu])
	if not path then 
		local oops = composition:AskUser("No clip in Loader. Halting Script.", {})
		error("No clip")
	end
	
	-- Verify that a file exists at the path
	if path then
		local f = bmd.readdir(path)
		if not f[1] then 
			local oops = composition:AskUser("Clip not found on disk. Halting Script.", {
				{"", "Text", ReadOnly = true, Lines = 1, Default = path}
			})
			error("File not found")
		end
	end 

	-- Store the original path in CustomData, in case it needs to be retrieved later.
	tool:SetData("source", path)

	-- Sanitize the input
	path = string.gsub(path, "[/\\]", SEPARATOR)
	if path then dprint("\nSantized full path to asset: "..path) end
	dprint("SEPARATOR = "..SEPARATOR)

	local elemtable 
	-- Create a table from the clip's path.
	if path then elemtable = split(path, SEPARATOR) end
	dprint("\nelemtable:")
	ddump(elemtable)

	-- Store current Trim points
	local trimIn = tool.ClipTimeStart[tu]
	local trimOut = tool.ClipTimeEnd[tu]
	local globalIn = tool.GlobalIn[tu]
	local globalOut = tool.GlobalOut[tu]
	local holdFirst = tool.HoldFirstFrame[tu]
	local holdLast = tool.HoldLastFrame[tu]

	-- Initialize the variable that determines whether the asset is an image sequence
	-- or a single file.
	local sequence = 1

	-- Find last entry in table
	local k = table.getn(elemtable)

	-- The default element name is the same as the filename, which is the last 
	-- item in the table.
	local elemname = elemtable[k]
	local elfile = elemtable[k]
	dprint("Element filename: "..elfile)
	
	-- Remove the filename entry from the table.
	elemtable[k] = nil

	-- find last ., which should indicate file extension
	local i = 0
	while i do
		k = i
		i = string.find(elfile, "%.", i+1)
	end

	-- Scan and store file extension
	local extension = string.lower(string.sub(elfile, k+1))
	
	-- Trim extension from elemname
	elemname = string.sub(elemname, 1, k-1)
	dprint("Extension removed: "..elemname)
	
	-- Compare extension against the table
	-- If it matches, this isn't an image sequence
	for i,j in ipairs(VIDEO) do 
		if j == extension then
			sequence = 0
			-- Video files should not get unique sub-folders
			elemname = VIDEOFOLDER
			dprint("Video format detected: "..extension)
		end
	end

	-- Check for single frame element
	if trimOut == trimIn then 
		sequence = 0 
		-- single frame elements should not get unique sub-folders
		elemname = STILLSFOLDER
		dprint("Single frame element detected.")
	else
		-- Remove frame numbers from elemname
		elemname = string.gsub(elemname, "%d+$", '')
		for key, val in ipairs(FRAMEDELIMITERS) do
			elemname = string.gsub(elemname, "%"..val.."$", '')
		end
		dprint("Frame numbers removed: "..elemname)
	end

	-- Get the comp's file name
	local compfile = composition:GetAttrs().COMPS_FileName

	-- Make sure the file's been saved
	if compfile == '' then 
		local oops = composition:AskUser("Please save your composition first. Halting Script.", {})
		error("No comp file")
	end
	dprint("\nComp filename: "..compfile)

	local fp
	-- Create a table from the composition's file path
	if compfile then fp = parseFilename(compfile) end

	local pathtable = split(comp:MapPath(fp.FullPath), SEPARATOR)
	dprint("Path table to comp file")
	ddump(pathtable)
	
	-- Remove the comp filename from the table
	pathtable[table.getn(pathtable)] = nil

	-- This is the distance from the file system root to the comp file
	local pathdepth = 0

	for i,j in ipairs(pathtable) do
		pathdepth = i
	end
	
	dprint("pathdepth = "..pathdepth)


	local currentPath = ""
	local targetPath = ""

	-- Reconstuct the paths of both the shot and the asset
	for i=1, pathdepth-COMPDEPTH do
		if pathtable[i] then targetPath = targetPath..pathtable[i]..SEPARATOR end
		if elemtable[i] then currentPath = currentPath..elemtable[i]..SEPARATOR end
	end
	dprint("currentPath = "..currentPath)
	dprint("targetPath = "..targetPath)

	-- If the paths match, then the asset is already in the shot folder 
	-- (although it may still not be in the right place).
	if string.lower(currentPath) == string.lower(targetPath) then
		local oops = composition:AskUser("Asset is already local. Aborting.", {})
		error("Already Local")
	end

	-- Create a dialog box
	local dialog = composition:AskUser("", { 
		{"Element Subfolder", "Text", Lines = 1, Default = elemname},
		{"Sequence", "Checkbox", Default = sequence},
		})
		
	if not dialog then 
		error("User clicked Cancel. Script halted.")	
	end
		
	path = ""

	-- Reconstruct the path to the asset. We need to do this because we previously
	-- removed the filename from the end.
	for i,j in ipairs(elemtable) do
		if j then path = path..j..SEPARATOR end
	end
	dprint("\nReconstructed source path: "..path)

	local newlocation = ""

	-- Construct a path to the asset's target location
	for i=1, pathdepth-COMPDEPTH do
		if pathtable[i] then newlocation = newlocation .. pathtable[i].. SEPARATOR end
	end

	--trim terminal slash
	newlocation = newlocation:sub(1,-2)
	

	if not dialog["Element Subfolder"] then dialog["Element Subfolder"] = '' end 
	
	-- Add separators to subfolder names where necessary
	if ASSETFOLDER == '' then
		-- do nothing
	else 
	dprint("adding a slash")
		ASSETFOLDER = SEPARATOR..ASSETFOLDER
	end
	
	if dialog["Element Subfolder"] == '' then
		-- do nothing
	else
		dialog["Element Subfolder"] = SEPARATOR..dialog["Element Subfolder"]
	end
	
	newlocation = newlocation ..ASSETFOLDER..dialog["Element Subfolder"]
	dprint("\nNew path to asset: "..newlocation)
	
	local windowsMakeDir = "md \""..newlocation.."\""
	local macMakeDir = "mkdir -p \'"..newlocation.."\'"
	local linuxMakeDir = "mkdir -p \'"..newlocation.."\'"
	
	local windowsCopySequence = "copy \""..path.."\" \""..newlocation..SEPARATOR.."\""
	local macCopySequence = "cp -R \""..path.."\" \""..newlocation..SEPARATOR.."\""
	local linuxCopySequence = "cp -R \""..path.."\" \""..newlocation..SEPARATOR.."\""
	
	local windowsCopyFile = "copy \""..path..elfile.."\" \""..newlocation..SEPARATOR..elfile.."\""
	local macCopyFile = "cp \""..path..elfile.."\" \""..newlocation..SEPARATOR..elfile.."\""
	local linuxCopyFile = "cp \""..path..elfile.."\" \""..newlocation..SEPARATOR..elfile.."\""

	dprint("\nCommand construction: \nWindows:")
	dprint(windowsMakeDir)
	dprint(windowsCopyFile)
	dprint(windowsCopySequence)
	dprint("Mac:")
	dprint(macMakeDir)
	dprint(macCopyFile)
	dprint(macCopySequence)
	dprint("Linux:")
	dprint(linuxMakeDir)
	dprint(linuxCopyFile)
	dprint(linuxCopySequence)

	-- Create directory
	if PLATFORM == "Windows" then
		os.execute(windowsMakeDir)
		dprint("Executing os command:")
		dprint(windowsMakeDir)
	elseif PLATFORM == "Mac" then
		os.execute(macMakeDir)
		dprint("Executing os command:")
		dprint(macMakeDir)
	elseif PLATFORM == "Linux" then
		os.execute(linuxMakeDir)
		dprint("Executing os command:")
		dprint(linuxMakeDir)
	end
	
	if not bmd.readdir(newlocation)[1] then 
		local oops = composition:AskUser("ERROR: New folder not created.", {
			{"", "Text", ReadOnly = true, Lines = 1, Default = newlocation}
		})
		error("New folder not created.")
	end

	dprint("\nTarget folder details:")
	ddump(bmd.readdir(newlocation))

	-- Copy the asset
	if dialog["Sequence"] == 1 then
		print("\nCopying Sequence")
		if PLATFORM == "Windows" then
			os.execute(windowsCopySequence)
			dprint("Executing os command:")
			dprint(windowsCopySequence)
		elseif PLATFORM == "Mac" then
			os.execute(macCopySequence)
			dprint("Executing os command:")
			dprint(macCopySequence)
		elseif PLATFORM == "Linux" then		
			os.execute(linuxCopySequence)	
			dprint("Executing os command:")
			dprint(linuxCopySequence)
		end
	else
		print("\nCopying File")
		if PLATFORM == "Windows" then
			os.execute(windowsCopyFile)
			dprint("Executing os command:")
			dprint(windowsCopyFile)
		elseif PLATFORM == "Mac" then
			os.execute(macCopyFile)
			dprint("Executing os command:")
			dprint(macCopyFile)
		elseif PLATFORM == "Linux" then		
			os.execute(linuxCopyFile)
			dprint("Executing os command:")
			dprint(linuxCopyFile)
		end	
	end

	-- Set the new path in the Loader
	tool.Clip = comp:ReverseMapPath(newlocation..SEPARATOR..elfile)
	dprint("\nNew Clip value: "..comp:ReverseMapPath(newlocation..SEPARATOR..elfile))

	path = comp:MapPath(tool.Clip[tu])
	
	if path then
		local f = bmd.readdir(path)
		if not f[1] then 
			local oops = composition:AskUser("File not found at new location.", {
				{"", "Text", ReadOnly = true, Lines = 1, Default = path}
			})
			error("Unexpected failure. File not found at new location.")
		end
	end

	-- Restore the In/Out points
	dprint("Setting In/Out Points:")
	dprint("Global Out = "..globalOut)
	dprint("Global In = "..globalIn)
	dprint("Trim In = "..trimIn+1)
	dprint("Trim Out = "..trimOut+1)
	dprint("Hold First = "..holdFirst)
	dprint("Hold Last = "..holdLast.."\n")
	tool.GlobalOut[tu] = globalOut
	tool.GlobalIn[tu] = globalIn
	tool.ClipTimeStart[tu] = trimIn
	tool.ClipTimeEnd[tu] = trimOut
	tool.HoldFirstFrame[tu] = holdFirst
	tool.HoldLastFrame[tu] = holdLast

end -- end of main()

-- Utility function to split a string at a delimiter character.
function split(strInput, delimit)
	local strLength
	local strTemp
	local strCollect
	local tblSplit
	local intCount

	tblSplit = {}
	intCount = 0
	strCollect = ""
	if delimit == nil then
		delimit = ","
	end

	strLength = string.len(strInput)
	for i = 1, strLength do
		strTemp = string.sub(strInput, i, i)
		if strTemp == delimit then
			intCount = intCount + 1
			tblSplit[intCount] = trim(strCollect)
			strCollect = ""
		else
			strCollect = strCollect .. strTemp
		end
	end
	intCount = intCount + 1
	tblSplit[intCount] = trim(strCollect)

	return tblSplit
end -- end of split()

-- Utility function to extract information from a file name.
function parseFilename(filename)
	local seq = {}
	seq.FullPath = filename
	string.gsub(seq.FullPath, "^(.+[/\\])(.+)", function(path, name) seq.Path = path seq.FullName = name end)
	string.gsub(seq.FullName, "^(.+)(%..+)$", function(name, ext) seq.Name = name seq.Extension = ext end)

	if not seq.Name then -- no extension?
		seq.Name = seq.FullName
	end

	string.gsub(seq.Name,     "^(.-)(%d+)$", function(name, SNum) seq.CleanName = name seq.SNum = SNum end)

	if seq.SNum then
		seq.Number = tonumber( seq.SNum )
		seq.Padding = string.len( seq.SNum )
	else
	   seq.SNum = ""
	   seq.CleanName = seq.Name
	end

	if seq.Extension == nil then seq.Extension = "" end
	seq.UNC = ( string.sub(seq.Path, 1, 2) == [[\\]] )

	return seq
end -- end of parseFilename()


--========================== DEBUGGING ============================--


---------------------------------------------------------------------
-- dprint(string, suppressNewline)
--
-- Prints debugging information to the console when DEBUG flag is set
--
-- Arguments:
--		string, string, a message for the console
--		suppressNewline, boolean, do not start a new line
---------------------------------------------------------------------
function dprint(string, suppressNewline)
	local newline = "\n"
	if suppressNewline then newline = '' end
	if DEBUG then comp:Print(string..newline) end
end -- dprint()

---------------------------------------------------------------------
-- ddump(object)
--
-- Performs a dump() if the DEBUG flag is set
--
-- Arguments
--		object, object, an object to be dumped
---------------------------------------------------------------------
function ddump(object)
	if DEBUG then dump(object) end
end -- end ddump()

main()
