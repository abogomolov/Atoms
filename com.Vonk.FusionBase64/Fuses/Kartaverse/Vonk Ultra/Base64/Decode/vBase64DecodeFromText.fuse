-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil

-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vBase64DecodeFromText"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = DATATYPE,
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Base64\\Decode",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Base64 decodes text from a Fusion Text object.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    -- [[ Creates the user interface. ]]
    InText = self:AddInput("Text", "Text" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "TextEditControl",
        TEC_Lines = 5,
        LINK_Main = 1,
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        if param.Value == 1.0 then
            InText:SetAttrs({LINK_Visible = true})
        else
            InText:SetAttrs({LINK_Visible = false})
        end
    end
end

function Process(req)
    -- [[ Creates the output. ]]
    local base64_str = InText:GetValue(req).Value
    local str = base64utils.base64decode(base64_str)

    local out = Text(str)

    OutText:Set(req, out)
end
