-- ============================================================================
-- modules
-- ============================================================================
local base64utils = self and require("vbase64utils") or nil
local textutils = self and require("vtextutils") or nil


-- ============================================================================
-- constants
-- ============================================================================
FUSE_NAME = "vMetadataToComp"
DATATYPE = "Text"

-- ============================================================================
-- fuse
-- ============================================================================
FuRegisterClass(FUSE_NAME, CT_Tool, {
    REGID_DataType = DATATYPE,
    REGID_InputDataType = "Image",
    REG_NoCommonCtrls = true,
    REGS_Category = "Kartaverse\\Vonk Ultra\\Meta\\Comp",
    REGS_Name = FUSE_NAME,
    REGS_OpDescription = "Creates a Fusion comp from image metadata.",
    REGS_OpIconString = FUSE_NAME
})

function Create()
    InImage = self:AddInput("Input", "Input", {
        LINKID_DataType = "Image",
        -- INPID_InputControl = "ImageControl",
        LINK_Main = 1,
    })

    InFile = self:AddInput("File" , "File" , {
        LINKID_DataType = "Text",
        INPID_InputControl = "FileControl",
        FC_IsSaver = true,
        FC_ClipBrowse = false,
        LINK_Visible = false,
        FCS_FilterString =  "Fusion Composite (*.comp)|*.comp",
        LINK_Main = 2
    })

    InShowInput = self:AddInput("Show Input", "ShowInput", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 1.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    InShowDump = self:AddInput("Show Dump", "ShowDump", {
        LINKID_DataType = "Number",
        INPID_InputControl = "CheckboxControl",
        INP_Integer = true,
        INP_Default = 0.0,
        INP_External = false,
        INP_DoNotifyChanged = true
    })

    -- The output node connection where data is pushed out of the fuse
    OutText = self:AddOutput("Output", "Output", {
        LINKID_DataType = "Text",
        LINK_Main = 1,
    })
end

function NotifyChanged(inp, param, time)
    --[[
        Handles all input control events.

        :param inp: Input that triggered a signal.
        :type inp: Input

        :param param: Parameter object holding the (new) value.
        :type param: Parameter

        :param time: Current frame number.
        :type time: float
    ]]
    if inp == InShowInput then
        local visible
        if param.Value == 1.0 then visible = true else visible = false end

        -- InImage:SetAttrs({LINK_Visible = visible})
        InFile:SetAttrs({LINK_Visible = visible})
    end
end

-- The OnAddToFlow() function is automatically run when the a new fuse node is added to the comp, or when the .comp file is opened up.
function OnAddToFlow()
end

function Process(req)
    -- [[ Creates the output. ]]
    local image_in = InImage:GetValue(req)

    local rel_path = InFile:GetValue(req).Value
    local abs_path = self.Comp:MapPath(rel_path)

    local show_dump = InShowDump:GetValue(req).Value

    -- Decode the comp
    local comp_str = ""
    local tbl = image_in.Metadata
    if tbl and tbl.CompB64 then
        -- Base64 encoded comp
        local base64_str = tbl.CompB64
        local decoded_str = base64utils.base64decode(base64_str)
        comp_str = decoded_str or ""
    elseif tbl and tbl.Comp then
        -- Lua table encoded comp
        comp_str = bmd.writestring(tbl.Comp) or ""
    end

    -- Write the comp to disk if there is a filename specified
    if rel_path ~= "" then
        textutils.write_file(abs_path, comp_str)
    end

    if show_dump == 1 then
        print("\n[ScriptVal Lua Table]")
        dump(tbl)
        print("\n[ScriptVal Text Output]")
        dump(comp_str)
    end

    local out = Text(comp_str)
    OutText:Set(req , out)
end
